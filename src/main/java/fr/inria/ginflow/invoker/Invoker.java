/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.invoker;

import fr.inria.hocl.core.hocli.*;

import java.lang.ProcessBuilder.Redirect;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible to call the actual service.
 * 
 * @author: msimonin
 * 
 */
public class Invoker {

    /** The logger. */
    final static Logger log = LoggerFactory.getLogger(Invoker.class);

    /*
     * 
     * Invoke the command with its parameters. parameters list is constructed by
     * parsing the Molecule and concatenated every String/ExternalObject found
     */
    public static Solution call(String sName, String command,
            Molecule inputParameters, Molecule moleculeEnvironment) {
        // We take all the String/ExternalObjects as inputParameters
        String parameters = "";
        SimpleIterator<Atom> mainIterator = inputParameters.newIterator();
        Atom atomIterator;
        while ((atomIterator = mainIterator.next()) != null) {
            if (atomIterator instanceof ExternalObject) {
                ExternalObject externalParameter = (ExternalObject) atomIterator;
                if (externalParameter.getObject() instanceof String) {
                    String parameter = (String) externalParameter.getObject();
                    parameters += " " + parameter;
                }
            }
        }

        Map<String, String> environment = deconstruct(moleculeEnvironment);
                
        String operationName = command + " " + parameters;

        Molecule m = new Molecule();
        // here the solution will be :
        // <"OUT":<,,,,>, "ERR":<,,,,>, "EXIT":<,,,,>>
        Solution outSolution = new Solution();
        Molecule outMolecule = new Molecule();

        Solution errSolution = new Solution();
        Molecule errMolecule = new Molecule();

        Solution exitSolution = new Solution();
        Molecule exitMolecule = new Molecule();

        Solution sNameSolution = new Solution();
        Molecule sNameMolecule = new Molecule();

        String output = "";
        String errorOutput = "";
        int exitStatus = -1;

        ProcessBuilder pb = new ProcessBuilder(Arrays.asList("/bin/bash", "-c",
                operationName));
        pb.redirectInput(Redirect.INHERIT);
        
        Map<String, String> env = pb.environment();
        /**We add the $home/bin directory to the path*/ 
        String path = env.get("PATH");
        env.put("PATH", path + ":"  + env.get("HOME") + "/bin");

        /**We all defined environment variables.*/
        env.putAll(environment);

        try {
            log.info(String.format("Invoking command %s",
                    operationName));
            log.debug(String.format("Invoking command %s with env %s", operationName, env));
            Process p = pb.start();

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new InputStreamReader(
                    p.getErrorStream()));

            // read the output from the command
            String s = "";
            while ((s = stdInput.readLine()) != null) {
                output += s;
            }

            // read the error output from the command
            while ((s = stdError.readLine()) != null) {
                errorOutput += s;
            }

            // read the exit status
            exitStatus = p.waitFor();

        } catch (Exception e) {
            errorOutput += e.getMessage();
        }

        // out solution
        outMolecule.add(new ExternalObject(output));
        outSolution.addMolecule(outMolecule);
        Tuple tOut = new Tuple(2);
        m.add(tOut);
        tOut.set(0, new ExternalObject("OUT"));
        tOut.set(1, outSolution);

        // err solution
        errMolecule.add(new ExternalObject(errorOutput));
        errSolution.addMolecule(errMolecule);
        Tuple tErr = new Tuple(2);
        m.add(tErr);
        tErr.set(0, new ExternalObject("ERR"));
        tErr.set(1, errSolution);

        // exit status
        exitMolecule.add(new ExternalObject(exitStatus));
        exitSolution.addMolecule(exitMolecule);
        Tuple tExit = new Tuple(2);
        m.add(tExit);
        tExit.set(0, new ExternalObject("EXIT"));
        tExit.set(1, exitSolution);

        // provenance solution
        sNameMolecule.add(new ExternalObject(sName));
        sNameSolution.addMolecule(sNameMolecule);
        Tuple tName = new Tuple(2);
        m.add(tName);
        tName.set(0, new ExternalObject("NAME"));
        tName.set(1, sNameSolution);

        Solution s = new Solution();
        s.addMolecule(m);

        return (s);
    } // call

    public static Map<String, String> deconstruct(Molecule molecule) {
        Map<String, String> result = new HashMap<String, String>();
        if (molecule == null) {
            return result;
        }
        Atom atomIterator;
        SimpleIterator<Atom> mainIterator = molecule.newIterator();
        while ((atomIterator = mainIterator.next()) != null) {
            if (atomIterator instanceof ExternalObject) {
                ExternalObject externalParameter = (ExternalObject) atomIterator;
                if (externalParameter.getObject() instanceof String) {
                    String r = (String) externalParameter.getObject();
                    String[] split = r.split("=");
                    if (split.length == 2) {
                        result.put(split[0], split[1]);
                    }
                }
            }
        }
        return result;
    }
} // Invoker
