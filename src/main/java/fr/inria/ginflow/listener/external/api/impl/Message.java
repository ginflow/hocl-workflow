/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.listener.external.api.impl;

public class Message {
	
	public static final String RESULT_MESSAGE = "RESULT";
	public static final String WORKFLOW_MESSAGE = "WORKFLOW_STARTED";
	public static final String DEPLOYMENT_SUCCES_MESSAGE = "DEPLOYMENT_SUCCESS";
	public static final String DESCRIPTION_SENT = "DESCRIPTION_SENT";
	
	private String type_;
	
	private String message_;

	public Message(String type, String message) {
		type_ = type;
		message_ = message;
	}

	public String getType() {
		return type_;
	}

	public void setType(String type) {
		type_ = type;
	}

	public String getMessage() {
		return message_;
	}

	public void setMessage(String message) {
		message_ = message;
	}
	
	
}
