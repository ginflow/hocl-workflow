/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.cli.handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import fr.inria.ginflow.cli.command.CommonCommand;
import fr.inria.ginflow.options.OptionException;
import fr.inria.ginflow.options.Options;

/**
 * @author msimonin
 *
 */
public class CommonCommandHandler {

    /** The logger. */
    final static Logger log = LoggerFactory
            .getLogger(CommonCommandHandler.class);

    CommonCommand command_;

    public CommonCommandHandler(CommonCommand command) {
        super();
        command_ = command;
    }

    /**
     * 
     * Configure the logger.
     * 
     * @throws JoranException	JoranException
     */
    protected void configureLogger() throws JoranException {
        int level = command_.getDebug();
        LoggerContext loggerContext = (LoggerContext) LoggerFactory
                .getILoggerFactory();
        loggerContext.reset();
        JoranConfigurator configurator = new JoranConfigurator();
        ClassLoader cl = this.getClass().getClassLoader();
        URL url = cl.getResource(String.format("logback-%s.xml", level));
        configurator.setContext(loggerContext);
        configurator.doConfigure(url);
    }

    /**
     * 
     * Loads the options.
     * Creates a default option set then override with 
     * the options in the configuration file if possible,
     * finally override the options whith those given on the command line (-o switch)
     * 
     * 
     * @return	The options
     * @throws OptionException	OptionException
     */
    protected Options loadOptions() throws OptionException {
        // load default
        Options defaultOptions = new Options();
        // attempt to load from file
        try{
            Options options = Options.fromFile(command_.getConfigFile());
            defaultOptions.putAll(options);
        } catch (Exception e) {
            log.debug("Unable to read the configuration from the file {}", command_.getConfigFile());
        }
        // get the options from the command line
        Map<String, String> cliOptions = command_.getExecutorOptions();
        defaultOptions.putAll(cliOptions);
        log.debug("{}", defaultOptions);
        return defaultOptions;
    }
}
