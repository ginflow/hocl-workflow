/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.external.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.GinflowType;
import fr.inria.ginflow.internal.api.RebranchingInformation;
import fr.inria.ginflow.internal.api.Workflow;

/**
 * 
 * Build a new workflow.
 * 
 * @author msimonin
 *
 */
public class WorkflowBuilder {

    public List<RebranchingInformation> getRebranchingInformations() {
		return rebranchingInformations_;
	}

	/** Workflow name. */
    private String name_;

    /** Keep track of all the services. */
    private Map<String, ServiceBuilder> services_;

    /** Keep track of the explicit rebranching information.*/
    private List<RebranchingInformation> rebranchingInformations_;
    
    public static WorkflowBuilder newWorkflowBuilder() {
        return new WorkflowBuilder();
    }

    private WorkflowBuilder() {
        super();
        services_ = new HashMap<String, ServiceBuilder>();
        rebranchingInformations_ = new ArrayList<RebranchingInformation>();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name_;
    }

    /**
     * Sets the name of the workflow builder.
     * 
     * @param name the name to set
     * @return	The workflow builder under construction
     */
    public WorkflowBuilder setName(String name) {
        name_ = name;
        return this;
    }

    /**
     * @return the services
     */
    public Map<String, ServiceBuilder> getServices() {
        return services_;
    }

  
    /**
     * @param services	The service to set
     * @return	The workflow builder under construction
     */
    public WorkflowBuilder setServices(Map<String, ServiceBuilder> services) {
        services_ = services;
        return this;
    }

    public WorkflowBuilder addServiceBuilder(ServiceBuilder serviceBuilder) {
        services_.put(serviceBuilder.getName(), serviceBuilder);
        return this;
    }

    public Workflow build() throws GinflowException {
    	if (name_ == null) {
            throw new GinflowException(GinflowException.MUST_GIVE_A_NAME);
        }
        Workflow workflow = Workflow.newWorkflow(name_, UUID.randomUUID().toString());
        return build(workflow);
    }
    
    	
    public Workflow build(String workflowId) throws GinflowException {
    	if (name_ == null) {
            throw new GinflowException(GinflowException.MUST_GIVE_A_NAME);
        }
        Workflow workflow = Workflow.newWorkflow(name_, workflowId);
    	return build(workflow);
    }
    
    private Workflow build(Workflow workflow) throws GinflowException {        

        workflow.setRebranchingInformations(rebranchingInformations_);
        // check if the service is declared as an alternative
        Map<String, Set<String>> updateSrc = new HashMap<String, Set<String>>();
        Map<String, Set<String>> updateDst = new HashMap<String, Set<String>>();
        List<String> supervised = new ArrayList<String>();
        // keep tracks of the different rebranching information
        Map<String, RebranchingInformation> rebranchingInformation = new HashMap<String, RebranchingInformation>(); 
        
        try {
            for (Entry<String, ServiceBuilder> entry : services_.entrySet()) {
                ServiceBuilder sBuilder = entry.getValue();
                GinflowTuple service = sBuilder.build();
                String serviceName = sBuilder.getName();

                // check if the service is terminal
                // now it should be handle by register service
//                if ((sBuilder.get(GinflowType.DST) == null
//                        || sBuilder.get(GinflowType.DST).isEmpty()
//                     ) && (sBuilder.get(GinflowType.DST_CONTROL) == null
//                        || sBuilder.get(GinflowType.DST_CONTROL).isEmpty())) {
//                    workflow.getTerminalServices().add(serviceName);
//                }
                // check if the service is supervised,
                if (sBuilder.isSupervised()) {
                	// update the rebranching information
                	addSupervised(serviceName, sBuilder.getSupervisedGroup(), rebranchingInformation);
                    //rebranchingInformation.addSupervised(serviceName);
                }

                if (sBuilder.isAlternative()) {
                    // add new dest to the sources of the alternative
                    List<Object> sources = sBuilder.get(GinflowType.SRC);
                    if (sources != null) {
                        for (Object source : sources) {
                            String src = (String) source;
                            ServiceBuilder srcService = services_.get(src);
                            if (!srcService.isAlternative()) {
                                // This service will potentially update its
                                // destination to the alternative in case of
                                // adaptiveness.
                            	addUpdateSrc(serviceName, src, sBuilder.getAlternativeGroup() , rebranchingInformation);
                                //rebranchingInformation.addUpdateSrc(src, serviceName);
                            }
                        }
                    }

                    List<Object> sourcesControl = sBuilder.get(GinflowType.SRC_CONTROL);
                    if (sourcesControl != null) {
                        for (Object source : sourcesControl) {
                            String src = (String) source;
                            ServiceBuilder srcService = services_.get(src);
                            if (!srcService.isAlternative()) {
                                // This service will potentially update its
                                // destination to the alternative in case of
                                // adaptiveness.
                            	addUpdateSrcControl(src, serviceName, sBuilder.getAlternativeGroup() , rebranchingInformation);
                                //rebranchingInformation.addUpdateSrcControl(src, serviceName);
                            }
                        }
                    }
                    
                    // add new source to the destinations of the alternative
                    List<Object> destinations = sBuilder.get(GinflowType.DST);
                    if (destinations != null) {
                        for (Object destination : destinations) {
                            String dst = (String) destination;
                            ServiceBuilder dstService = services_.get(dst);
                            if (!dstService.isAlternative()) {
                                // This service will potentially update its
                                // source in case of adaptiveness.
                            	addUpdateDst(dst, serviceName, sBuilder.getAlternativeGroup() , rebranchingInformation);
                                //rebranchingInformation.addUpdateDst(dst, serviceName);
                            }
                        }
                    }
                    List<Object> destinationsControl = sBuilder.get(GinflowType.DST_CONTROL);
                    if (destinationsControl != null) {
                        for (Object destination : destinationsControl) {
                            String dst = (String) destination;
                            ServiceBuilder dstService = services_.get(dst);
                            if (!dstService.isAlternative()) {
                                // This service will potentially update its
                                // source in case of adaptiveness.
                            	addUpdateDstControl(dst, serviceName, sBuilder.getAlternativeGroup() , rebranchingInformation);
                                //rebranchingInformation.addUpdateDstControl(dst, serviceName);
                            }
                        }
                    }
                }
                // register the service
                workflow.registerService(serviceName, service);
            }
            // update workflow decoration
            // TODO : handle several rebranching
            List<RebranchingInformation> workflowRebranching = new ArrayList<RebranchingInformation>();
            workflowRebranching.addAll(rebranchingInformation.values());
            // add those rebranching
            List<RebranchingInformation> ris = workflow.getRebranchingInformations();
            ris.addAll(workflowRebranching);
            workflow.setRebranchingInformations(ris);
        } catch (Exception e) {
            throw new GinflowException(e);
        }

        return workflow;
    }

	public void setRebranchingInformations(List<RebranchingInformation> rebranchingInformations) {
		rebranchingInformations_ = rebranchingInformations;
	}

	private void addSupervised(String serviceName, String groupKey,
			Map<String, RebranchingInformation> rebranchingInformation) {
		RebranchingInformation ri = getOrCreateRebranchingInformation(groupKey, rebranchingInformation);
		ri.addSupervised(serviceName);
		
	}

	private void addUpdateSrc(String serviceName, String src, String groupKey,
			Map<String, RebranchingInformation> rebranchingInformation) {
		RebranchingInformation ri = getOrCreateRebranchingInformation(groupKey, rebranchingInformation);
		ri.addUpdateSrc(src, serviceName);
		
	}

	private void addUpdateSrcControl(String src, String serviceName, String groupKey,
			Map<String, RebranchingInformation> rebranchingInformation) {
		RebranchingInformation ri = getOrCreateRebranchingInformation(groupKey, rebranchingInformation);
		ri.addUpdateSrcControl(src, serviceName);
		
	}

	private void addUpdateDst(String dst, String serviceName, String groupKey,
			Map<String, RebranchingInformation> rebranchingInformation) {
		RebranchingInformation ri = getOrCreateRebranchingInformation(groupKey, rebranchingInformation);
		ri.addUpdateDst(dst, serviceName);
		
	}

	private void addUpdateDstControl(String dst, String serviceName, String groupKey,
			Map<String, RebranchingInformation> rebranchingInformation) {
		RebranchingInformation ri = getOrCreateRebranchingInformation(groupKey, rebranchingInformation);
		ri.addUpdateDstControl(dst, serviceName);
	}

	private RebranchingInformation getOrCreateRebranchingInformation(String groupKey,
			Map<String, RebranchingInformation> rebranchingInformation) {
		RebranchingInformation ri = rebranchingInformation.get(groupKey);
		if (ri == null) {
			ri = new RebranchingInformation();
			rebranchingInformation.put(groupKey, ri);
		}
		return ri;
	}

	public void addRebranchingInformation(RebranchingInformation ri) {
		rebranchingInformations_.add(ri);
		
	}
}
