/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.external.json;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.deser.std.StdDeserializer;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;

import fr.inria.ginflow.external.api.ServiceBuilder;

/**
 * 
 * Service deserializer.
 * 
 * @author msimonin
 * 
 */
public class ServiceBuilderDeserializer extends StdDeserializer<ServiceBuilder> {

    private static final String FIELD_NAME = "name";

    private static final String FIELD_ALT = "alt";

    private static final String FIELD_SUP = "sup";

    protected ServiceBuilderDeserializer(Class<?> vc) {
        super(vc);
    }

    public ServiceBuilderDeserializer(JavaType valueType) {
        super(valueType);
    }

    @Override
    public ServiceBuilder deserialize(JsonParser jp,
            DeserializationContext context) throws IOException,
            JsonProcessingException {
        ServiceBuilder sBuilder = ServiceBuilder.newBuilder();
        if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("invalid start marker");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldname = jp.getCurrentName();

            jp.nextToken(); // move to next token in string

            if (FIELD_ALT.equals(fieldname)) {
                sBuilder.setAlternativeGroup(jp.getIntValue());
//                jp.nextToken();
            } else if (FIELD_SUP.equals(fieldname)) {
                sBuilder.setSupervisedGroup(jp.getIntValue());
//                jp.nextToken();
            } else {
                /**
                 * all other specific fields. There are assumed to be in shape :
                 * "key": [foo, bar]
                 * */
                List<Object> objects = jp
                        .readValueAs(new TypeReference<List<String>>() {
                        });
                
                // Particular case for name field : we set the name to sBuilder
                sBuilder.addAll(fieldname.toUpperCase(), objects);
                if (FIELD_NAME.equals(fieldname)) {
                    sBuilder.setName((String) objects.get(0));
                }
            }
        }
        return sBuilder;
    }
}
