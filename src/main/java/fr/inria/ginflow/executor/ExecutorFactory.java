/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor;

import java.util.Map;

import fr.inria.ginflow.distributed.deployer.api.WorkflowDeployer;
import fr.inria.ginflow.distributed.deployer.api.impl.mesos.MesosWorkflowDeployer;
import fr.inria.ginflow.distributed.deployer.api.impl.ssh.SSHWorkflowDeployer;
import fr.inria.ginflow.distributed.enacter.api.WorkflowEnacter;
import fr.inria.ginflow.distributed.enacter.api.impl.DistributedWorkflowEnacter;
import fr.inria.ginflow.executor.api.GinflowExecutor;
import fr.inria.ginflow.executor.api.impl.CentralisedExecutor;
import fr.inria.ginflow.executor.api.impl.DistributedExecutor;
import fr.inria.ginflow.executor.api.impl.DistributedWorkflowModifier;
import fr.inria.ginflow.executor.api.impl.WorkflowModifier;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.options.Options;

/**
 * 
 * Executor Factory constucts executors.
 * 
 * @author msimonin
 * 
 */
public class ExecutorFactory {

    /**
     * @param workflow					The workflow
     * @param executorName				Executor name picked in {central, mesos, ssh ...}
     * @param executorOptions			The options
     * @return The Ginflow Executor.	
     */
    public static GinflowExecutor newExecutor(Workflow workflow,
            String executorName, Map<String, String> executorOptions) {
        GinflowExecutor executor = new CentralisedExecutor(workflow, executorOptions);
        if (executorName.toLowerCase().equals(GinflowExecutor.CENTRAL)) {
            executor = new CentralisedExecutor(workflow, executorOptions);
        } else if (executorName.toLowerCase().equals(GinflowExecutor.MESOS)) {
        	WorkflowModifier workflowModifier = DistributedWorkflowModifier.getInstance();
        	WorkflowDeployer workflowDeployer = new MesosWorkflowDeployer(workflow, executorOptions);
        	WorkflowEnacter workflowEnacter = new DistributedWorkflowEnacter(workflow, executorOptions);
        	executor = new DistributedExecutor(workflow, executorOptions, workflowModifier, workflowDeployer, workflowEnacter);
        } else if (executorName.toLowerCase().equals(GinflowExecutor.SSH)) {
        	WorkflowModifier workflowModifier = DistributedWorkflowModifier.getInstance();
        	WorkflowDeployer workflowDeployer = new SSHWorkflowDeployer(workflow, executorOptions);
        	WorkflowEnacter workflowEnacter = new DistributedWorkflowEnacter(workflow, executorOptions);
            executor = new DistributedExecutor(workflow, executorOptions, workflowModifier, workflowDeployer, workflowEnacter);
        } else if (executorName.toLowerCase().equals(GinflowExecutor.NOOP)) {
        	WorkflowModifier workflowModifier = DistributedWorkflowModifier.getInstance();
        	// create a noop deployer
        	WorkflowDeployer workflowDeployer = new SSHWorkflowDeployer(workflow, executorOptions);
        	WorkflowEnacter workflowEnacter = new DistributedWorkflowEnacter(workflow, executorOptions);
            executor = new DistributedExecutor(workflow, executorOptions, workflowModifier, workflowDeployer, workflowEnacter);
        }
        return executor;
    }



}
