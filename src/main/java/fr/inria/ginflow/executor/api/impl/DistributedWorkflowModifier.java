/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import fr.inria.ginflow.internal.api.Adapt;
import fr.inria.ginflow.internal.api.AdaptDst;
import fr.inria.ginflow.internal.api.AdaptDstControl;
import fr.inria.ginflow.internal.api.AdaptSrc;
import fr.inria.ginflow.internal.api.AdaptSrcControl;
import fr.inria.ginflow.internal.api.RebranchingInformation;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.rules.distributed.Gw_adapt;
import fr.inria.ginflow.rules.distributed.Gw_call;
import fr.inria.ginflow.rules.distributed.Gw_prepare_send_block;
import fr.inria.ginflow.rules.distributed.Gw_receive;
import fr.inria.ginflow.rules.distributed.Gw_receive_control;
import fr.inria.ginflow.rules.distributed.Gw_send;
import fr.inria.ginflow.rules.distributed.Gw_send_control;
import fr.inria.ginflow.rules.distributed.Gw_setup;
import fr.inria.ginflow.rules.distributed.Gw_swap_dst;
import fr.inria.ginflow.rules.distributed.Gw_swap_src;
import fr.inria.hocl.core.hocli.Atom;
import fr.inria.hocl.core.hocli.SimpleIterator;

public class DistributedWorkflowModifier implements WorkflowModifier {

	
	private static DistributedWorkflowModifier INSTANCE = new DistributedWorkflowModifier();
	
	/**Higher level listener.*/
	private WorkflowListener workflowListener_;
	
	private DistributedWorkflowModifier(){
		
	}
	
	public static DistributedWorkflowModifier getInstance() {
		// TODO Auto-generated method stub
		return INSTANCE;
	}
	
	/**
	 * 
	 * Decorate a workflow before execution.
	 * 
	 * @param workflow	The workflow
	 */
	public void decorate(Workflow workflow) {
		// handle (static) adaptiveness
		prepareAllRebranchings(workflow);
		// add other rules
		addCommonRules(workflow);
	}

	
	public void addCommonRules(Workflow workflow) {
		SimpleIterator<Atom> it = workflow.newIterator();
		Service service;
		// Common rules.
		while ((service = (Service) it.next()) != null) {
			service.addRule(new Gw_call());
			service.addRule(new Gw_setup());
			// on error continue / stop ?
			// TODO : add an option ?
			service.addRule(new Gw_prepare_send_block());
			service.addRule(new Gw_send());
			service.addRule(new Gw_receive());
			service.addRule(new Gw_receive_control());
			service.addRule(new Gw_send_control());
		}
	}
	
	public void prepareAllRebranchings(Workflow workflow) {
		List<RebranchingInformation> rebranchingInformations = workflow.getRebranchingInformations();

		for (RebranchingInformation rebranchingInformation : rebranchingInformations) {
			prepareRebranching(workflow, rebranchingInformation);
		}
	}

	private void prepareRebranching(Workflow workflow, RebranchingInformation rebranchingInformation) {

		List<Object> adaptDestinations = new ArrayList<Object>();
		Service service;

		// 1. we add the adapt rules which will send ADAPT molecule
		adaptDestinations.addAll(rebranchingInformation.getUpdateDst().keySet());
		adaptDestinations.addAll(rebranchingInformation.getUpdateSrc().keySet());
		adaptDestinations.addAll(rebranchingInformation.getUpdateDstControl().keySet());
		adaptDestinations.addAll(rebranchingInformation.getUpdateSrcControl().keySet());

		// service to swap sources (destination of the alternatives)
		Set<String> swapDsts = new LinkedHashSet<String>();
		swapDsts.addAll(rebranchingInformation.getUpdateDst().keySet());
		swapDsts.addAll(rebranchingInformation.getUpdateDstControl().keySet());

		// service to swap dsts (sources of the alternatives)
		Set<String> swapSrcs = new LinkedHashSet<String>();
		swapSrcs.addAll(rebranchingInformation.getUpdateSrc().keySet());
		swapSrcs.addAll(rebranchingInformation.getUpdateSrcControl().keySet());

		for (String sName : rebranchingInformation.getSupervised()) {
			service = workflow.getOrSetService(sName);
			service.setAdapt(Adapt.newAdapt().addAll(adaptDestinations));
			service.addRule(new Gw_adapt());
		}
		// 2. We add the swap src rules
		// This will add the following molecules :
		// * "ADAPT_SRC":<new set of data sources>
		// * "ADAPT_SRC_CONTROL":<new set of control sources>
		// * gw_swap_src which will swap all sources.
		for (String sName : swapDsts) {
			service = workflow.getService(sName);
			List<Object> adaptSrcs = new ArrayList<Object>();
			List<Object> adaptSrcsControl = new ArrayList<Object>();
			if (rebranchingInformation.getUpdateDst().get(sName) != null) {
				adaptSrcs.addAll(rebranchingInformation.getUpdateDst().get(sName));
			}

			if (rebranchingInformation.getUpdateDstControl().get(sName) != null) {
				adaptSrcsControl.addAll(rebranchingInformation.getUpdateDstControl().get(sName));
			}
			service = workflow.getOrSetService(sName);
			service.setAdaptSrc(AdaptSrc.newAdaptSrc().addAll(adaptSrcs));
			service.setAdaptSrcControl(AdaptSrcControl.newAdaptSrcControl().addAll(adaptSrcsControl));
			service.addRule(new Gw_swap_src());

		}

		// 3. We add the swap dst rules
		// This will add the following molecules :
		// * "ADAPT_DST":<new set of data destinations>
		// * "ADAPT_DST_CONTROL":<new set of control destinations>
		// * gw_swap_dst which will swap all destinations.
		for (String sName : swapSrcs) {
			service = workflow.getOrSetService(sName);
			List<Object> adaptDsts = new ArrayList<Object>();
			List<Object> adaptDstsControl = new ArrayList<Object>();
			if (rebranchingInformation.getUpdateSrc().get(sName) != null) {
				adaptDsts.addAll(rebranchingInformation.getUpdateSrc().get(sName));
			}

			if (rebranchingInformation.getUpdateSrcControl().get(sName) != null) {
				adaptDstsControl.addAll(rebranchingInformation.getUpdateSrcControl().get(sName));
			}
			service = workflow.getOrSetService(sName);
			service.setAdaptDst(AdaptDst.newAdaptDst().addAll(adaptDsts));
			service.setAdaptDstControl(AdaptDstControl.newAdaptDstControl().addAll(adaptDstsControl));
			service.addRule(new Gw_swap_dst());
		}
	}

	@Override
	public void setWorkflowListener(WorkflowListener workflowListener) {
		workflowListener_ = workflowListener;
	}
	
	
}
