/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl.dist;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.DestinationMolecule;
import fr.inria.ginflow.distributed.PutMolecule_Thread;
import fr.inria.ginflow.distributed.communication.CommunicationFactory;
import fr.inria.ginflow.distributed.communication.api.CentralMultisetListener;
import fr.inria.ginflow.distributed.communication.api.CentralMultisetPublisher;
import fr.inria.ginflow.distributed.enacter.api.impl.DistributedWorkflowEnacter;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.ServiceListener;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.options.Options;
import fr.inria.hocl.core.hocli.HocliWorkflow;
import fr.inria.hocl.core.hocli.Molecule;

/**
 * 
 * CentralMultiset is the main entry point for the multiset managing the
 * workflow.
 * 
 * @author hfernandez, msimonin
 *
 */
public class CentralMultiset implements ServiceListener {

    /** The logger. */
    final static Logger log_ = LoggerFactory.getLogger(CentralMultiset.class);

    private Workflow workflow_;

    private String workflowId_;

    private WorkflowListener listener_;

    private boolean started_ = false;

    private Map<String, String> options_;

    private Map<String, GinflowTuple[]> terminals_;

    private CentralMultisetListener centralMultisetListener_;

	private CentralMultisetPublisher centralMultisetPublisher_;


    public CentralMultiset(Workflow workflow, String workflowId,
            Map<String, String> options, WorkflowListener listener) {

        super();
        log_.debug("Creating a new CentralMultiset");
        workflow_ = workflow;
        workflow.getValue();
        options_ = options;
        listener_ = listener;
        // This code should be deprecated asap
        //
        HocliWorkflow.workflowId = workflowId;
        workflowId_ = workflowId;
        HocliWorkflow.setVerboseLevel(Integer.valueOf(options
                .get(Options.HOCL_DEBUG_LEVEL)));

        terminals_ = new HashMap<String, GinflowTuple[]>();

        log_.debug("CentralMultiset initialized");

    }

    /**
     * Start to publish and receive results. We don't tolerate to receive
     * results before all the initial description are at least ready to be sent
     * (and copied see : 
     * https://bitbucket.org/ginflow/hocl-workflow/issue/60/kafka-broker-sequence-with-more-than-32)
     * 
     */
    public synchronized void start() {
        if (started_) {
            log_.debug("Workflow already started ... skipping");
            return;
        }
        
        try {
            centralMultisetListener_ = CommunicationFactory
                    .newCentralMultisetListener(workflowId_, options_, this);
            centralMultisetListener_.listen();

            centralMultisetPublisher_ = CommunicationFactory
                    .newCentralMultisetPublisher(workflowId_,
                            options_, this);
            centralMultisetPublisher_.publish(workflow_);
            
        } catch (Exception e) {
            log_.error("Error when publishing the initial description", e);
        }
        started_ = true;
    }

    /**
     * 
     * Restart the multiset. 
     * If the underlying message communication layer is persistent (kafka)
     * This should be fine.
     * 
     */
    public void restart() {
    	log_.debug("Restarting the multiset");
    	if (started_) {
    		log_.debug("Workflow already started ... skipping");
    		return;
    	}
        centralMultisetListener_ = CommunicationFactory
                .newCentralMultisetListener(workflowId_, options_, this);
        // start consuming the outstanding message.
        centralMultisetListener_.listen();
        started_ = true;
    }
    
	/**
	 * 
	 * Update the running workflow 
	 * 
	 * @param workflowUpdate	The workflow update description.
	 */
	public void update(Workflow workflowUpdate) {
		if (!started_) {
			log_.info("The central multiset isn't started yet !");
			return;
		}
		// register the services
		for (String sName : workflowUpdate.getServicesNames()) {
			// TODO register only new services...
			workflow_.registerService(sName, workflowUpdate.getService(sName));
		}
        centralMultisetPublisher_.publish(workflowUpdate);
	}
    
    /**
     * 
     * Update the Workflow description accordingly to the freshly received
     * destinationMolecule.
     * 
     * @param destMol
     *            The destination molecule.
     */
    public synchronized void onServiceUpdated(DestinationMolecule destMol) {
        String destination = destMol.getDestination();
        String origin = destMol.getOrigen();
        Molecule molecule = destMol.getMol();

        log_.debug(String.format("Received a molecule" + "origin : %s \n"
                + "destination : %s \n", origin, destination));
        log_.debug(String.format("Received molecule : %s", molecule));

        if (origin.equals(destination)) {
            // we update the sub sol
            PutMolecule_Thread p = new PutMolecule_Thread(workflow_,
                    destMol.getMol(), destMol.getDestination(),
                    destMol.getOrigen(), this);
            p.start();
        } else {
            // we transfer the molecule to the destination.
            // -> the multiset centralizes every communication.
            /*
             * (optimization) the service is resonsible of sending the molecule
             * to the right topic.
             */

            // this.sendMolecule(destination, molecule);
        }
        log_.debug("Updating the multiset");
    }

    public void onResultReceived(Service service) {
        log_.info("-> Service {} done", service.getName());
        listener_.onResultReceived(service);
        if (workflow_.getTerminalServices().contains(service.getName())) {
            // we update the terminals with the result
            terminals_.put(service.getName(), service.getResult());
            log_.info("-> Result received {}/{}", terminals_.size(), workflow_
                    .getTerminalServices().size());
            if (terminals_.size() == workflow_.getTerminalServices().size()) {
                listener_.onWorkflowTerminated();
                try {
                    centralMultisetListener_.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    log_.error("Error when closing the listeners", e);
                }
            }
        }
        
    }

	@Override
	public void onServiceDescriptionSent(GinflowTuple service) {
		listener_.serviceDescriptionSent(service);
	}

}
