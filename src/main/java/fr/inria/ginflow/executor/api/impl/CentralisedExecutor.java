/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl;

import java.util.Map;

import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.executor.api.GinflowExecutor;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.options.Options;
import fr.inria.hocl.core.hocli.HocliWorkflow;

/**
 * 
 * The workflow execution is centralised.
 * 
 * @author msimonin
 * 
 */
public class CentralisedExecutor extends GinflowExecutor {

    /** The workflow to execute. */
    private Workflow workflow_;
    
    /** Executor options. */
    private Map<String, String> options_;

	private CentralisedWorkflowModifier workflowModifier_;
    
    public CentralisedExecutor(Workflow workflow, Map<String, String> options) {
        workflow_ = workflow;
        options_ = options;
        workflowModifier_ =  CentralisedWorkflowModifier.getInstance();
    }


    @Override
    public void execute() {
    	workflowModifier_.decorate(workflow_);
        HocliWorkflow.setVerboseLevel(Integer.valueOf(options_.get(Options.HOCL_DEBUG_LEVEL)));
        int size =  workflow_.getServicesNames().size();
        if (workflow_.getServicesNames().size() > 1) {
            workflow_.getValue().setNonInert();
            workflow_.getValue().reduce();
        } else if (size == 1) {
            String sName = workflow_.getServicesNames().get(0);
            GinflowTuple service = workflow_.getService(sName);
            service.getValue().setNonInert();
            service.getValue().reduce();
        } else  {
            // nothing to do
        }
    }

	@Override
	public void update(Workflow workflowUpdate) throws GinFlowExecutorException {
		// TODO Auto-generated method stub
	}


	@Override
	public void restart() {
		// TODO Auto-generated method stub
		
	}



}
