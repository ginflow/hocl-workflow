/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl.dist;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.DestinationMolecule;
import fr.inria.ginflow.distributed.PendingMoleculeConsumer;
import fr.inria.ginflow.distributed.ReduceSolutionFlag;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.ginflow.distributed.communication.CommunicationFactory;
import fr.inria.ginflow.distributed.communication.CommunicationType;
import fr.inria.ginflow.distributed.communication.api.ServiceInvokerListener;
import fr.inria.ginflow.distributed.communication.api.SimplePublisher;
import fr.inria.ginflow.options.OptionException;
import fr.inria.ginflow.options.Options;
import fr.inria.hocl.core.hocli.Molecule;
import fr.inria.hocl.core.hocli.Solution;

/**
 * 
 * Service agent.
 * 
 * @author msimonin
 * 
 */
public class ServiceInvoker {

    /** Logger. */
    private static final Logger log_ = LoggerFactory
            .getLogger(ServiceInvoker.class);

    private String workflowId_;

    private String serviceId_;

    private Solution sol_;

    private Object reduceReady;

    /** The batch consumer. */
    private PendingMoleculeConsumer pendingMoleculeConsumer_;

    /** Pending molecules (waiting for the batch consumer to consume). */
    private BlockingQueue<Molecule> pendingMolecules_;

    /** The options. */
    private Options options_;

    private ServiceInvokerListener serviceInvokerListener_;

    private SimplePublisher serviceInvokerPublisher_;

   
    /**
     * For factorization purpose.
     * 
     * @param workflowId		The workflow id.
     * @param serviceId			The service id.
     * @throws OptionException	OptionException
     */
    public ServiceInvoker(String workflowId, String serviceId)
            throws OptionException {
        workflowId_ = workflowId;
        serviceId_ = serviceId;
        sol_ = new Solution();
        reduceReady = new Object();
        options_ = new Options();
        pendingMolecules_ = new LinkedBlockingQueue<Molecule>();
        pendingMoleculeConsumer_ = new PendingMoleculeConsumer(sol_,
                reduceReady, pendingMolecules_);
        pendingMoleculeConsumer_.start();
    }

    private void initializeBrokerConnection() throws Exception {
        // TODO pass in the args.
        serviceInvokerListener_ = CommunicationFactory
                .newServiceInvokerListener(workflowId_, serviceId_, options_,
                        pendingMolecules_);
        serviceInvokerListener_.listen();
        serviceInvokerPublisher_ = CommunicationFactory.newSimplePublisher(options_);
        
        TransferMolecule.setWorkflowId(workflowId_);
        TransferMolecule.setPublisher(serviceInvokerPublisher_);
        TransferMolecule.setOrigen(serviceId_);
        
    }

    public ServiceInvoker(String workflowId, String serviceId, String[] args)
            throws Exception {
        // we construct the local options
        this(workflowId, serviceId);
        Map<String, String> localOptions = analizeArguments(args);
        options_.putAll(localOptions);
        log_.debug("options : {}", options_);
        initializeBrokerConnection();
        // TODO Auto-generated constructor stub
    }

    public void start() throws Exception {
        // for now HocliWorkflow must be initialized.
        // TODO remove this hidden dep.
        initializeChaos();
        synchronized (reduceReady) {
            while (true) {
                while (!ReduceSolutionFlag.reduce) {
                    // free the lock
                    log_.debug("Waiting to be notified for reducing");
                    reduceReady.wait();
                }
                // A molecule has been received we reduce.
                // sol.setNonInert();
                sol_.reduce();
                log_.info("Reduced solution", sol_);
                if (checkResultTuple(sol_.contents)) {
                    try {
                    	// send it to central multiset
                        serviceInvokerPublisher_
                                .publish(new DestinationMolecule(serviceId_,
                                        sol_.getContents().clone(), serviceId_), CommunicationType.toMultisetQueue(workflowId_));
                        // ChWSPublisher oChWSPublisher = new ChWSPublisher(
                        // connection, sol_.getContents().clone(),
                        // serviceId_, serviceId_);
                        // oChWSPublisher.run();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                ReduceSolutionFlag.reduce = false;
                reduceReady.notify();
            } // while (don't free the lock yet)
        } // synchronized

    }

    private void initializeChaos() {
        final long sleep = Long.valueOf(options_.get(Options.CHAOS_SLEEP));
        final double threshold = Double.valueOf(options_.get(Options.CHAOS_THRESHOLD));
        if (threshold <= 0){
            return;
        }
        Thread t = new Thread(){
            public void run(){
                //simulate a kill ?
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    log_.error(e.getMessage());
                }
                Random random = new Random();
                double rand = random.nextDouble();
                if (rand <  threshold) {
                    System.exit(143);
                }
            }
        };
        t.start();
    }

    private boolean checkResultTuple(Molecule contents) {
        return true;
    }

    private Map<String, String> analizeArguments(String[] args)
            throws OptionException {
        Map<String, String> localOptions = new HashMap<String, String>();
        int size = args.length;
        // we are only interested in -o options
        for (int i = 0; i < size; i++) {
            if (args[i].equals("-o")) {
                i++;
                if (i >= size || args[i].charAt(0) == '-') {
                    throw new OptionException("invalid argument list : " + args);
                } else {
                    // optionString : key=value
                    String[] option = args[i].split("=", 2);
                    if (option.length != 2) {
                        throw new OptionException("invalid option format: "
                                + option);
                    }
                    localOptions.put(option[0], option[1]);
                }
            }
        }
        return localOptions;
    }

}
