/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.communication.CommunicationFactory;
import fr.inria.ginflow.distributed.communication.api.CentralMultisetUpdateListener;
import fr.inria.ginflow.distributed.deployer.api.WorkflowDeployer;
import fr.inria.ginflow.distributed.enacter.api.WorkflowEnacter;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.executor.api.GinflowExecutor;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.listener.external.api.ExternalWorkflowListener;

/**
 * 
 * The workflow excecution is distributed. TODO add listener / events.
 * 
 * @author msimonin
 * 
 */
public class DistributedExecutor 
	extends GinflowExecutor {

    /** The logger. */
    final static Logger log_ = LoggerFactory.getLogger(DistributedExecutor.class);
	
	/** The workflow to execute. */
	protected Workflow workflow_;
	
	protected Map<String, String> options_;
	
	/** The update Listener.*/
	private CentralMultisetUpdateListener updateListener_;

	protected WorkflowModifier workflowModifier_;

	private WorkflowDeployer workflowDeployer_;

	private WorkflowEnacter workflowEnacter_;
		
	public DistributedExecutor(
			Workflow workflow,
			Map<String, String> options,
			WorkflowModifier workflowModifier,
			WorkflowDeployer workflowDeployer,
			WorkflowEnacter workflowEnacter
			) {
		workflow_ = workflow;
		options_ = options;
		String queueId = workflow.getId();
		/** Will trigger the update method of the implementation (SSH / Mesos ...)*/
		updateListener_ = CommunicationFactory.newCentralMultisetUpdateListener(queueId, options, this);
		updateListener_.listen();
		// Get the workflow modifier instance.
		workflowModifier_ = workflowModifier;
		workflowModifier_.setWorkflowListener(this);
		workflowDeployer_ = workflowDeployer;
		workflowDeployer_.setWorkflowListener(this);
		workflowEnacter_ = workflowEnacter;
		workflowEnacter_.setWorkflowListener(this);
	}

	@Override
	public void execute() throws GinFlowExecutorException{
		workflowModifier_.decorate(workflow_);
		workflowDeployer_.deploy(workflow_);
		workflowEnacter_.enact(workflow_);
		workflowDeployer_.clean();
	}
	
	/** 
	 * It's like starting the new workflow except that :
	 * 
	 *  - we don't have to start all the services
	 *  - we don't have to add all the common rules to existing services.
	 *  
	 *  In addition we prevent two updates to happen concurrently.
	 * @throws GinFlowExecutorException
	 *  
	 * TODO : can we use the same logic as start ? 
	 * 
	 * */
	@Override
	public synchronized void update(Workflow workflowUpdate) throws GinFlowExecutorException{
		// first, add common rules
    	workflowModifier_.addCommonRules(workflowUpdate);
		workflowDeployer_.deploy(workflowUpdate);
		// We should validate everything before.
		workflowModifier_.prepareAllRebranchings(workflowUpdate);
		workflowEnacter_.update(workflowUpdate);		
	}
	
	/**
	 * 
	 * It's like starting the workflow, assuming it has been already deployed.
	 * we just redecorate the workflow and restart the enactement.
	 * @throws GinFlowExecutorException 
	 * 
	 */
	@Override
	public void restart() throws GinFlowExecutorException {
		workflowModifier_.addCommonRules(workflow_);
		workflowEnacter_.restart();
	}

}
