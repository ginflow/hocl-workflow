/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl;

import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listenable.WorkflowListenable;

/**
 * 
 * Encapsulate the modifications that will be operated on the workflow.
 * Warning : side effects in here.
 * 
 * @author msimonin
 *
 */
public interface WorkflowModifier extends WorkflowListenable {

	/**
	 * 
	 * Decorates the workflow.
	 * In most of the case it's about
	 * <ul> 
	 * 	<li> add(ing)CommonRules, followed by</li>
	 *  <li> prepare(ing)AllRebranchings (adaptiveness)</li>
	 * </ul>
	 * 
	 * @param workflow	The workflow
	 */
	public void decorate(Workflow workflow);
	
	/**
	 * 
	 * Add all the common rules that allows the workflow to run.
	 * 
	 * @param workflow	The workflow
	 */
	public void addCommonRules(Workflow workflow);
	
	/**
	 * 
	 * Add all the rules and molecules that will allow the adaptiveness
	 * 
	 * @param workflow	The workflow
	 */
	public void prepareAllRebranchings(Workflow workflow);

	

}
