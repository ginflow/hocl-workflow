/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api.impl.ssh;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.deployer.api.Command;
import fr.inria.ginflow.executor.api.impl.dist.ssh.listener.DeploymentListener;

public class GanySshRemoteExecutor extends RemoteExecutor 
{

    /** Logger. */
    private static final Logger log_ = LoggerFactory.getLogger(GanySshRemoteExecutor.class);
    
    
    /** The username.*/
    private String username_;
    
    /** The password.*/
    private String password_;

    /** polling interval.*/
    private int pollingTimeout_;
    
    /** Connection pool*/
    private GanySSHConnectionPool pool_;

    /** Public key.*/
    private File privateKey_;


	private DeploymentListener listener_;

    /**
     * 
     * Constructor.
     * 
     * @param username  Username
     * @param password  Password
     * @param listener  The deployment listener
     */
    public GanySshRemoteExecutor(
    		String username,
    		String password, 
    		DeploymentListener listener) 
    {
        username_ = username;
        password_ = password;
        // default value
        pollingTimeout_ = 60000;
        pool_ = new GanySSHConnectionPool(username_, password_);
        listener_ = listener;
    }
    
    
    public GanySshRemoteExecutor(
    		String username, 
    		File privateKey, 
    		String password,
    		DeploymentListener listener) {
        pollingTimeout_ = 60000;
        username_ = username;
        privateKey_ = privateKey;
        password_ = password;
        pool_ = new GanySSHConnectionPool(username_, privateKey_, password_);
        listener_ = listener;
    }

 
    public int getPollingTimeout() 
    {
        return pollingTimeout_;
    }

    public void setPollingTimeout(int pollingTimeout) 
    {
        pollingTimeout_ = pollingTimeout * 1000;
    }


    @Override
    public void execParallel(List<String> hosts, List<Command> commands) throws Exception
    {
        // TODO limit the parallelization.
        log_.debug(String.format("Run %d commands in parallel on %d hosts", commands.size(), hosts.size()));
        // nothing to do
        if (hosts.size() == 0 )
        {
            return ;
        }

            ExecutorService executor = Executors.newFixedThreadPool(hosts.size());
            ArrayList<Future<?>> futures = new ArrayList<Future<?>>();
            for (int i = 0; i < hosts.size() ; i++)
            {
                String host = hosts.get(i);
                Command command = commands.get(i);
                GanySSHExecutorWorker executorWorker = new GanySSHExecutorWorker(
                        host,
                        command,
                        pollingTimeout_,
                        pool_,
                        listener_);
                
                Future<?> future = executor.submit(executorWorker);
                futures.add(future);
            }
            
            executor.shutdown();
            log_.debug("Waiting termination");
            
            for (Future<?> future : futures) {
                future.get();
            }
        
    }

    @Override
    public void close() 
    {
        pool_.close();
    }


    @Override
    public void execParallel(List<String> hosts, Command command) throws Exception {
        List<Command> commands = new ArrayList<Command>();
        for (@SuppressWarnings("unused") String h : hosts) {
            commands.add(command);
        }
        execParallel(hosts, commands);
    }


    @Override
    public void exec(String host, Command command) throws Exception {
        execParallel(Arrays.asList(host), Arrays.asList(command));
    }
    
}
