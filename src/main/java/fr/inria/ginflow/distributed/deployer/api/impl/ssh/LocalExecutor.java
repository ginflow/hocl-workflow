/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api.impl.ssh;

import fr.inria.ginflow.executor.api.impl.dist.ServiceInvoker;
import fr.inria.hocl.core.hocli.HocliWorkflow;

public class LocalExecutor {
    

    // TODO wrap logic.
    public static void main(String[] args) throws Exception {
        // TODO this is bad : 
        // we parse once the args to extract the workflowId, serviceId which might be used 
        //in other part of the code as public static field.
        // and a second time to extract localOptions.
        HocliWorkflow.init(args); 
        String workflowId = HocliWorkflow.workflowId;
        String serviceId =  HocliWorkflow.serviceId;
        ServiceInvoker serviceInvoker = new ServiceInvoker(workflowId, serviceId, args);
        serviceInvoker.start();
    }

}
