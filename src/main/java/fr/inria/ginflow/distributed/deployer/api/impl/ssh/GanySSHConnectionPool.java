/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api.impl.ssh;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.ethz.ssh2.Connection;

/**
 * 
 * Connection pool TODO: singleton.
 * 
 * @author msimonin
 *
 */
public class GanySSHConnectionPool {

    /** Logger. */
    private static final Logger log_ = LoggerFactory
            .getLogger(GanySSHConnectionPool.class);

    /** Connection pool. */
    private ConcurrentMap<String, Connection> connections_;

    /** Username. */
    private String username_;

    /** Password. */
    private String password_;

    /** private Key_ */
    private File privateKey_;

    public static GanySSHConnectionPool getInstance() {
        // TODO Auto-generated method stub
        return null;
    }

    public GanySSHConnectionPool(String username, String password) {
        connections_ = new ConcurrentHashMap<String, Connection>();
        username_ = username;
        password_ = password;
    }

    public GanySSHConnectionPool(String username, File privateKey,
            String password) {
        connections_ = new ConcurrentHashMap<String, Connection>();
        username_ = username;
        privateKey_ = privateKey;
        password_ = password;
    }

    /**
     * 
     * Gets the connection, create a new one otherwise
     * 
     * @param host			The host
     * @throws IOException	IOException
     * 
     * @return The connection (newly created if not found in the pool)
     */
    public synchronized Connection getConnection(String host)
            throws IOException {
        Connection sshClient = connections_.get(host);
        if (sshClient == null || !sshClient.isAuthenticationComplete()) {
            log_.debug(String.format(
                    "Connection to %s not found, create a new one ", host));
            sshClient = new Connection(host);
            sshClient.connect();
            boolean isAuthenticated = sshClient.authenticateWithPublicKey(
                    username_, privateKey_, password_);
            if (!isAuthenticated) {
                log_.error("Authentification failed ... this shouldn't happen");
                // throw smt
            }
            connections_.put(host, sshClient);
        } else {
            log_.debug("Reusing connection for {}", host);
        }
        return sshClient;
    }

    public void close() {
        log_.debug("Closing the connections");
        // Close all the clients
        for (Connection sshClient : connections_.values()) {
            try {
                sshClient.close();
            } catch (Exception e) {
                log_.error(String.format(
                        "Unable to close the connection to %s : %s",
                        sshClient.getHostname(), e.getMessage()));
            }
        }

    }

}
