/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.communication.CommunicationFactory;
import fr.inria.ginflow.distributed.communication.CommunicationType;
import fr.inria.ginflow.exceptions.CommunicationException;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.ServiceListener;
import fr.inria.hocl.core.hocli.Molecule;
import fr.inria.hocl.core.hocli.Solution;

/**
 * 
 * Handles initial transfer of services description. Initial description is
 * pushed into a queue which is consumed by a thread that handles the actual
 * transfer to the service invoker.
 * 
 * 
 * @author msimonin
 *
 */
public class CentralMultisetPublisher {

    /** The logger. */
    final static Logger log_ = LoggerFactory
            .getLogger(CentralMultisetPublisher.class);

    /** The options. */
    protected Map<String, String> options_;

    /** The workflow Id. */
    protected String workflowId_;

    private SimplePublisher serviceInvokerPublisher_;

    private BlockingQueue<InitialDescription> publishingQueue_;

	private ServiceListener listener_;

	private ThreadConsumer consumer_;

    public CentralMultisetPublisher(
    		String workflowId,
            Map<String, String> options,
            ServiceListener listener) {
        super();
        workflowId_ = workflowId;
        options_ = options;
        listener_ = listener;
        serviceInvokerPublisher_ = CommunicationFactory.newSimplePublisher(options);
        publishingQueue_ = new LinkedBlockingQueue<InitialDescription>();
        log_.debug("CentralMultisetPublisher initialized");
        consumer_ = new ThreadConsumer();
        consumer_.start();
        log_.debug("ThreadConsumer initialized");
    }

    public void publish(Workflow workflow) {
        int size = workflow.getServicesNames().size();
        log_.info("Initial description to {} services is being transmitted",
                size);
        for (int i = 0; i < size; i++) {
            String sName = workflow.getServicesNames().get(i);
            // we clone the content of the molecule to send :
            // https://bitbucket.org/ginflow/hocl-workflow/issue/60/kafka-broker-sequence-with-more-than-32
            Molecule molecule = (Molecule) workflow.getService(sName)
                    .getValue().contents.clone();
            publishingQueue_.add(new InitialDescription(sName, molecule));
            log_.debug("The service {} is ready to be sent", sName);
        }

    }

    /**
     * @throws CommunicationException	CommunicationException
     */
    public void close() throws CommunicationException {
        serviceInvokerPublisher_.close();
    }

    class InitialDescription {
        /** The service name. */
        private String serviceName_;

        /** The molecule which represent the initial state of the service. */
        private Molecule serviceDescription_;

        public InitialDescription(String serviceName,
                Molecule serviceDescription) {
            super();
            serviceName_ = serviceName;
            serviceDescription_ = serviceDescription;
        }

        /**
         * @return the serviceName
         */
        public String getServiceName() {
            return serviceName_;
        }

        /**
         * @param serviceName
         *            the serviceName to set
         */
        public void setServiceName(String serviceName) {
            serviceName_ = serviceName;
        }

        /**
         * @return the serviceDescription
         */
        public Molecule getServiceDescription() {
            return serviceDescription_;
        }

        /**
         * @param serviceDescription
         *            the serviceDescription to set
         */
        public void setServiceDescription(Molecule serviceDescription) {
            serviceDescription_ = serviceDescription;
        }

    }

    /**
     * 
     * Consumes from the publishing queue and sends the initial description
     * 
     * @author msimonin
     *
     */
    class ThreadConsumer extends Thread {
        private int size_;

		public ThreadConsumer() {
			setDaemon(true);
		}

		public void run() {
            log_.debug("Starting the publisher thread");
            boolean finished = false;
            int sent = 0;
            while (!finished) {

                try {
                    InitialDescription description = publishingQueue_.take();
                    String sName = description.getServiceName();
                    Molecule content = description.getServiceDescription();
                    serviceInvokerPublisher_.publish(
                            content,
                            CommunicationType.toServiceQueue(workflowId_, sName)
                            );
                    
                    // we publish the service
                    // TODO this must be simplified
                    Service service = Service.newService(sName);
                    Solution solution = new Solution();
                    solution.contents = content;
                    service.setValue(solution);
                    service.setName(sName);
                    log_.debug("Send initial description to {} ",
                            description.getServiceName());
                    listener_.onServiceDescriptionSent(service);
                    sent++;
                } catch (Exception e) {
                    finished = true;
                    e.printStackTrace();
                }
            }
            log_.debug("Publisher thread terminated");
        }
    }

}
