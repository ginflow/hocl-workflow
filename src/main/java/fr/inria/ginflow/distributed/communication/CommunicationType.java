/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication;

public class CommunicationType {
	
	public static String updateQueue(String workflowId) {
		return workflowId + ".update";
	}

	public static String fromServiceQueue(String workflowId, String dest) {
		return workflowId + "." + dest;
	}

	public static String toServiceQueue(String workflowId, String sName) {
		return fromServiceQueue(workflowId, sName);
	}

	public static String toMultisetQueue(String workflowId) {
		return workflowId;
	}
	
}
