/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api.impl.kafka;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.communication.api.SimplePublisher;
import fr.inria.ginflow.exceptions.CommunicationException;
import fr.inria.ginflow.options.Options;

/**
 * 
 * Publishes molecule either for the multiset or for another service.
 * 
 * @author msimonin
 *
 */
public class KafkaSimplePublisher extends SimplePublisher {

    /** The logger. */
    final static Logger log_ = LoggerFactory
            .getLogger(KafkaSimplePublisher.class);
    
    
    /** Kafka configuration options.*/
    private Properties props_;
    
    /** Producer.*/
    private KafkaProducer<String, Object> producer_;

    /**
     * 
     * Constructor.
     * 
     * @param options		The options.
     */
    public KafkaSimplePublisher(Map<String, String> options) {
        super(options);
        props_ = new Properties();
        props_.put("bootstrap.servers", options.get(Options.KAFKA_BOOTSTRAP_SERVERS));
        props_.put("acks", "1");
        
        producer_ = new KafkaProducer<String, Object>(
                props_,
                new StringSerializer(),
                new ObjectSerializer()
                );
    }

    @Override
    public void publish(Serializable molecule, String destination)
            throws Exception {
        // We should make sure to send molecules...
        ProducerRecord<String, Object> data = new ProducerRecord<String, Object>(destination, molecule);
        producer_.send(data);

    }

    @Override
    public void close() throws CommunicationException {
       producer_.close();

    }

}
