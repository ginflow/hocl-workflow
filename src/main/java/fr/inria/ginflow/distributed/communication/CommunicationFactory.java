/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.communication.api.CentralMultisetListener;
import fr.inria.ginflow.distributed.communication.api.CentralMultisetPublisher;
import fr.inria.ginflow.distributed.communication.api.CentralMultisetUpdateListener;
import fr.inria.ginflow.distributed.communication.api.ServiceInvokerListener;
import fr.inria.ginflow.distributed.communication.api.SimplePublisher;
import fr.inria.ginflow.distributed.communication.api.impl.activeMQ.ActiveMQCentralMultisetListener;
import fr.inria.ginflow.distributed.communication.api.impl.activeMQ.ActiveMQCentralMultisetUpdateListener;
import fr.inria.ginflow.distributed.communication.api.impl.activeMQ.ActiveMQServiceInvokerListener;
import fr.inria.ginflow.distributed.communication.api.impl.activeMQ.ActiveMQSimplePublisher;
import fr.inria.ginflow.distributed.communication.api.impl.kafka.KafkaCentralMultisetListener;
import fr.inria.ginflow.distributed.communication.api.impl.kafka.KafkaCentralMultisetUpdateListener;
import fr.inria.ginflow.distributed.communication.api.impl.kafka.KafkaServiceInvokerListener;
import fr.inria.ginflow.distributed.communication.api.impl.kafka.KafkaSimplePublisher;
import fr.inria.ginflow.distributed.communication.embedded.api.EmbeddedBroker;
import fr.inria.ginflow.distributed.communication.embedded.api.impl.ActiveMQEmbeddedBroker;
import fr.inria.ginflow.distributed.communication.embedded.api.impl.KafkaEmbeddedBroker;
import fr.inria.ginflow.listener.ServiceListener;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.options.Options;
import fr.inria.hocl.core.hocli.Molecule;

public class CommunicationFactory {

	/** The logger. */
	final static Logger log_ = LoggerFactory.getLogger(CommunicationFactory.class);

	public static CentralMultisetPublisher newCentralMultisetPublisher(String workflowId, Map<String, String> options,
			ServiceListener listener) {
		log_.debug("Creating a new Central Multiset Publisher");
		return new CentralMultisetPublisher(workflowId, options, listener);
	}

	public static CentralMultisetListener newCentralMultisetListener(String worflowId, Map<String, String> options,
			ServiceListener listener) {

		String broker = options.get(Options.BROKER).toLowerCase();
		log_.debug("Creating a new CentralMultisetListener : {}", broker);
		CentralMultisetListener centralMultisetListener = null;
		if (broker.equals("activemq")) {
			centralMultisetListener = new ActiveMQCentralMultisetListener(worflowId, options, listener);
		} else if (broker.equals("kafka")) {
			centralMultisetListener = new KafkaCentralMultisetListener(worflowId, options, listener);
		} else {
			centralMultisetListener = new ActiveMQCentralMultisetListener(worflowId, options, listener);
		}

		return centralMultisetListener;
	}

	public static ServiceInvokerListener newServiceInvokerListener(String workflowId, String serviceId, Options options,
			BlockingQueue<Molecule> pendingMolecules) {

		String broker = options.get(Options.BROKER).toLowerCase();
		log_.debug("Creating a new newServiceInvokerListener : {}", broker);

		ServiceInvokerListener serviceInvokerListener = null;
		if (broker.equals("activemq")) {
			serviceInvokerListener = new ActiveMQServiceInvokerListener(workflowId, serviceId, options,
					pendingMolecules);
		} else if (broker.equals("kafka")) {
			serviceInvokerListener = new KafkaServiceInvokerListener(workflowId, serviceId, options, pendingMolecules);
		} else {
			serviceInvokerListener = new ActiveMQServiceInvokerListener(workflowId, serviceId, options,
					pendingMolecules);
		}

		return serviceInvokerListener;

	}

	public static SimplePublisher newSimplePublisher(Map<String, String> options) {
		String broker = options.get(Options.BROKER).toLowerCase();
		log_.debug("Creating a new newServiceInvokerPublisher : {}", broker);

		SimplePublisher serviceInvokerPublisher = null;
		if (broker.equals("activemq")) {
			serviceInvokerPublisher = new ActiveMQSimplePublisher(options);
		} else if (broker.equals("kafka")) {
			serviceInvokerPublisher = new KafkaSimplePublisher(options);
		} else {
			serviceInvokerPublisher = new ActiveMQSimplePublisher(options);
		}

		return serviceInvokerPublisher;
	}

	public static CentralMultisetUpdateListener newCentralMultisetUpdateListener(String workflowId,
			Map<String, String> options, WorkflowListener listener) {
		String broker = options.get(Options.BROKER).toLowerCase();
		log_.debug("Creating a new CentralMultisetListener : {}", broker);
		CentralMultisetUpdateListener centralMultisetListener = null;
		if (broker.equals("activemq")) {
			centralMultisetListener = new ActiveMQCentralMultisetUpdateListener(workflowId, options, listener);
		} else if (broker.equals("kafka")) {
			centralMultisetListener = new KafkaCentralMultisetUpdateListener(workflowId, options, listener);
		} else {
			centralMultisetListener = new ActiveMQCentralMultisetUpdateListener(workflowId, options, listener);
		}

		return centralMultisetListener;
	}

	public static EmbeddedBroker newEmbeddedBroker(Map<String, String> options) {
		String broker = options.get(Options.BROKER).toLowerCase();
		log_.debug("Creating a new Embedded broker : {}", broker);
		EmbeddedBroker embeddedBroker = null;
		if (broker.equals("activemq")) {
			embeddedBroker = new ActiveMQEmbeddedBroker(options);
		} else if (broker.equals("kafka")) {
			embeddedBroker = new KafkaEmbeddedBroker(options);
		} else {
			embeddedBroker = new ActiveMQEmbeddedBroker(options);
		}
		return embeddedBroker;
	}

}
