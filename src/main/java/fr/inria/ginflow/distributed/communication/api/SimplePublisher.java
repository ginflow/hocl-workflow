/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api;

import java.io.Serializable;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.exceptions.CommunicationException;

public abstract class SimplePublisher {

    /** The logger. */
    final static Logger log_ = LoggerFactory
            .getLogger(SimplePublisher.class);

    /** The options. */
    protected Map<String, String> options_;

    public SimplePublisher(Map<String, String> options) {
        super();
        options_ = options;

    }

    public abstract void publish(Serializable message, String destination) throws Exception;

    public abstract void close() throws CommunicationException ;


}
