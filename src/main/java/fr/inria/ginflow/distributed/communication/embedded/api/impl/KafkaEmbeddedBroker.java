/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.embedded.api.impl;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Properties;

import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.ServerCnxnFactory;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.communication.embedded.api.EmbeddedBroker;
import fr.inria.ginflow.options.OptionException;
import fr.inria.ginflow.options.Options;
import kafka.server.KafkaConfig;
import kafka.server.KafkaServerStartable;

public class KafkaEmbeddedBroker implements EmbeddedBroker {

	/** Logger. */
	private static final Logger log_ = LoggerFactory.getLogger(KafkaEmbeddedBroker.class);

	/** The options. */
	private Map<String, String> options_;

	private KafkaServerStartable broker_;

	private String url_;

	private ServerCnxnFactory factory_;

	private File snapshotDir_;

	private File logDir_;

	/**
	 * 
	 * Constructor.
	 * 
	 * @param options	The options
	 */
	public KafkaEmbeddedBroker(Map<String, String> options) {
		log_.info("Starting a new kafka embedded broker");
		options_ = options;

        Properties properties = new Properties();
        properties.setProperty("zookeeper.connect", options.get(Options.KAFKA_ZOOKEEPER_CONNECT));
        properties.setProperty("broker.id", "0");
        // TODO handle list of servers
        String[] servers = options.get(Options.KAFKA_BOOTSTRAP_SERVERS).split(":");
        
        properties.setProperty("host.name", servers[0]);
        properties.setProperty("port", servers[1]);
        properties.setProperty("log.flush.interval.messages", String.valueOf(1));
        
		KafkaConfig kafkaConfig = new KafkaConfig(properties);
		broker_ = new KafkaServerStartable(kafkaConfig);
		
        // TODO handle list of servers
		String[] zkConnect = options.get(Options.KAFKA_ZOOKEEPER_CONNECT).split(":");
		log_.debug("Starting zookeeper on {}:{}", zkConnect[0], zkConnect[1]);
		try {
			factory_ = NIOServerCnxnFactory.createFactory(new InetSocketAddress(zkConnect[0], Integer.valueOf(zkConnect[1])), 1024);
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		snapshotDir_ = constructTempDir("embedded-zk/snapshot");
		logDir_ = constructTempDir("embedded-zk/log");
	}

	public KafkaEmbeddedBroker() throws OptionException {
		this(new Options());
	}

	/**
	 * Starting the broker
	 */
	@Override
	public void start() {
		try {
			factory_.startup(new ZooKeeperServer(snapshotDir_, logDir_, 500));
			broker_.startup();
		} catch (Exception e) {
			log_.error(e.getMessage());
		}

	}

	/**
	 * Stopping the broker.
	 */
	@Override
	public void stop() {
		broker_.shutdown();
		broker_.awaitShutdown();
		factory_.shutdown();
	}

	
	private File constructTempDir(String dirPrefix) {
        File file = new File(System.getProperty("java.io.tmpdir"), dirPrefix + Math.random() * 1000000);
        if (!file.mkdirs()) {
            throw new RuntimeException("could not create temp directory: " + file.getAbsolutePath());
        }
        file.deleteOnExit();
        return file;
    }

}
