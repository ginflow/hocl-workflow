/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.enacter.api;

import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listenable.WorkflowListenable;

public interface WorkflowEnacter extends WorkflowListenable {
	
	/**
	 * 
	 * Start the workflow.
	 * 
	 * @param workflow	The workflow
	 * @throws GinFlowExecutorException GinFlowExecutorException 
	 */
	void enact(Workflow workflow) throws GinFlowExecutorException;

	/**
	 * 
	 * Update a already running workflow
	 * 
	 * @param workflowUpdate	The workflow update-
	 */
	void update(Workflow workflowUpdate);

	/**
	 * Shutdown the enactor
	 */
	void shutdown();

	/**
	 * Restart the enacter.
	 * For now it could be use if the underlying communication layer is persistent.
	 * @throws GinFlowExecutorException 
	 */
	void restart() throws GinFlowExecutorException;
	

}
