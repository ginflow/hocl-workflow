/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_pass extends ReactionRule implements Serializable {

	
	public Gw_pass(){
		super("gw_pass", Shot.N_SHOT);
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray11;
		_HOCL_atomIteratorArray11 = new AtomIterator[2];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple9;
			_HOCL_atomIteratorArrayTuple9 = new AtomIterator[2];
			{
				class AtomIterator_t_j extends IteratorForExternal {
					public AtomIterator_t_j(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							satisfied = true;
						}
						return satisfied;
					}
				
				} // end of class AtomIterator_t_j
				_HOCL_atomIteratorArrayTuple9[0] = new AtomIterator_t_j();
			}
			{
				class IteratorSolution23 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray11;
						_HOCL_atomIteratorArray11 = new AtomIterator[2];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple10;
							_HOCL_atomIteratorArrayTuple10 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal9 extends IteratorForExternal {
									public AtomIterator__HOCL_literal9(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator14 = (IteratorForTuple)permutation.getAtomIterator(0);
											IteratorForSolution _HOCL_iteratorSolution5 = (IteratorForSolution)_HOCL_tupleAtomIterator14.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator15 = (IteratorForTuple)_HOCL_iteratorSolution5.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal9 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator15.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal9.equals("SRC");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal9
								_HOCL_atomIteratorArrayTuple10[0] = new AtomIterator__HOCL_literal9();
							}
							{
								class IteratorSolution19 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray11;
										_HOCL_atomIteratorArray11 = new AtomIterator[1];
										{
											class AtomIterator_t_i extends IteratorForExternal {
												public AtomIterator_t_i(){
													access = Access.REWRITE;
												}
												@Override
												public boolean conditionSatisfied() {
													Atom atom;
													boolean satisfied;
													atom = iterator.getElement();
													satisfied = false;
													if (atom instanceof ExternalObject
													  && ((ExternalObject)atom).getObject() instanceof String) {
														satisfied = true;
													}
													return satisfied;
												}
											
											} // end of class AtomIterator_t_i
											_HOCL_atomIteratorArray11[0] = new AtomIterator_t_i();
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray11 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray11[0] = new MoleculeIterator(1); // w_src
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray11, _HOCL_moleculeIteratorArray11);
										return perm;
									}
								
								} // class IteratorSolution19
								_HOCL_atomIteratorArrayTuple10[1] = new IteratorSolution19();
							}
							_HOCL_atomIteratorArray11[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple10, Gw_pass.this);
						}
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple11;
							_HOCL_atomIteratorArrayTuple11 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal10 extends IteratorForExternal {
									public AtomIterator__HOCL_literal10(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator16 = (IteratorForTuple)permutation.getAtomIterator(0);
											IteratorForSolution _HOCL_iteratorSolution6 = (IteratorForSolution)_HOCL_tupleAtomIterator16.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator17 = (IteratorForTuple)_HOCL_iteratorSolution6.getSubPermutation().getAtomIterator(1);
											String _HOCL_literal10 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator17.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal10.equals("IN");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal10
								_HOCL_atomIteratorArrayTuple11[0] = new AtomIterator__HOCL_literal10();
							}
							{
								class IteratorSolution21 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray12;
										_HOCL_atomIteratorArray12 = new AtomIterator[0];
										
										MoleculeIterator[] _HOCL_moleculeIteratorArray12 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray12[0] = new MoleculeIterator(1); // w_in
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray12, _HOCL_moleculeIteratorArray12);
										return perm;
									}
								
								} // class IteratorSolution21
								_HOCL_atomIteratorArrayTuple11[1] = new IteratorSolution21();
							}
							_HOCL_atomIteratorArray11[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple11, Gw_pass.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray13 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray13[0] = new MoleculeIterator(1); // w_j
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray11, _HOCL_moleculeIteratorArray13);
						return perm;
					}
				
				} // class IteratorSolution23
				_HOCL_atomIteratorArrayTuple9[1] = new IteratorSolution23();
			}
			_HOCL_atomIteratorArray11[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple9, Gw_pass.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple12;
			_HOCL_atomIteratorArrayTuple12 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_t_i_copy0 extends IteratorForExternal {
					public AtomIterator__HOCL_t_i_copy0(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator18 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_t_i_copy0 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator18.getAtomIterator(0)).getObject();
							IteratorForTuple _HOCL_tupleAtomIterator19 = (IteratorForTuple)permutation.getAtomIterator(0);
							IteratorForSolution _HOCL_iteratorSolution7 = (IteratorForSolution)_HOCL_tupleAtomIterator19.getAtomIterator(1);
							IteratorForTuple _HOCL_tupleAtomIterator20 = (IteratorForTuple)_HOCL_iteratorSolution7.getSubPermutation().getAtomIterator(0);
							IteratorForSolution _HOCL_iteratorSolution8 = (IteratorForSolution)_HOCL_tupleAtomIterator20.getAtomIterator(1);
							String t_i = (String)((IteratorForExternal)_HOCL_iteratorSolution8.getSubPermutation().getAtomIterator(0)).getObject();
							satisfied = _HOCL_t_i_copy0.equals(t_i);
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_t_i_copy0
				_HOCL_atomIteratorArrayTuple12[0] = new AtomIterator__HOCL_t_i_copy0();
			}
			{
				class IteratorSolution35 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray14;
						_HOCL_atomIteratorArray14 = new AtomIterator[2];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple13;
							_HOCL_atomIteratorArrayTuple13 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal11 extends IteratorForExternal {
									public AtomIterator__HOCL_literal11(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator21 = (IteratorForTuple)permutation.getAtomIterator(1);
											IteratorForSolution _HOCL_iteratorSolution9 = (IteratorForSolution)_HOCL_tupleAtomIterator21.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator22 = (IteratorForTuple)_HOCL_iteratorSolution9.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal11 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator22.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal11.equals("RES");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal11
								_HOCL_atomIteratorArrayTuple13[0] = new AtomIterator__HOCL_literal11();
							}
							{
								class IteratorSolution31 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray14;
										_HOCL_atomIteratorArray14 = new AtomIterator[1];
										{
											AtomIterator[] _HOCL_atomIteratorArrayTuple14;
											_HOCL_atomIteratorArrayTuple14 = new AtomIterator[2];
											{
												class AtomIterator__HOCL_literal12 extends IteratorForExternal {
													public AtomIterator__HOCL_literal12(){
														access = Access.REWRITE;
													}
													@Override
													public boolean conditionSatisfied() {
														Atom atom;
														boolean satisfied;
														atom = iterator.getElement();
														satisfied = false;
														if (atom instanceof ExternalObject
														  && ((ExternalObject)atom).getObject() instanceof String) {
															
															IteratorForTuple _HOCL_tupleAtomIterator23 = (IteratorForTuple)permutation.getAtomIterator(1);
															IteratorForSolution _HOCL_iteratorSolution10 = (IteratorForSolution)_HOCL_tupleAtomIterator23.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator24 = (IteratorForTuple)_HOCL_iteratorSolution10.getSubPermutation().getAtomIterator(0);
															IteratorForSolution _HOCL_iteratorSolution11 = (IteratorForSolution)_HOCL_tupleAtomIterator24.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator25 = (IteratorForTuple)_HOCL_iteratorSolution11.getSubPermutation().getAtomIterator(0);
															String _HOCL_literal12 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator25.getAtomIterator(0)).getObject();
															satisfied = _HOCL_literal12.equals("TRANSFER");
														}
														return satisfied;
													}
												
												} // end of class AtomIterator__HOCL_literal12
												_HOCL_atomIteratorArrayTuple14[0] = new AtomIterator__HOCL_literal12();
											}
											{
												class IteratorSolution29 extends IteratorForSolution {
													protected Permutation makeSubPermutation(){
														Permutation perm;
														AtomIterator[] _HOCL_atomIteratorArray14;
														_HOCL_atomIteratorArray14 = new AtomIterator[2];
														{
															AtomIterator[] _HOCL_atomIteratorArrayTuple15;
															_HOCL_atomIteratorArrayTuple15 = new AtomIterator[2];
															{
																class AtomIterator__HOCL_literal13 extends IteratorForExternal {
																	public AtomIterator__HOCL_literal13(){
																		access = Access.REWRITE;
																	}
																	@Override
																	public boolean conditionSatisfied() {
																		Atom atom;
																		boolean satisfied;
																		atom = iterator.getElement();
																		satisfied = false;
																		if (atom instanceof ExternalObject
																		  && ((ExternalObject)atom).getObject() instanceof String) {
																			
																			IteratorForTuple _HOCL_tupleAtomIterator26 = (IteratorForTuple)permutation.getAtomIterator(1);
																			IteratorForSolution _HOCL_iteratorSolution12 = (IteratorForSolution)_HOCL_tupleAtomIterator26.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator27 = (IteratorForTuple)_HOCL_iteratorSolution12.getSubPermutation().getAtomIterator(0);
																			IteratorForSolution _HOCL_iteratorSolution13 = (IteratorForSolution)_HOCL_tupleAtomIterator27.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator28 = (IteratorForTuple)_HOCL_iteratorSolution13.getSubPermutation().getAtomIterator(0);
																			IteratorForSolution _HOCL_iteratorSolution14 = (IteratorForSolution)_HOCL_tupleAtomIterator28.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator29 = (IteratorForTuple)_HOCL_iteratorSolution14.getSubPermutation().getAtomIterator(0);
																			String _HOCL_literal13 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator29.getAtomIterator(0)).getObject();
																			satisfied = _HOCL_literal13.equals("OUT");
																		}
																		return satisfied;
																	}
																
																} // end of class AtomIterator__HOCL_literal13
																_HOCL_atomIteratorArrayTuple15[0] = new AtomIterator__HOCL_literal13();
															}
															{
																class IteratorSolution25 extends IteratorForSolution {
																	protected Permutation makeSubPermutation(){
																		Permutation perm;
																		AtomIterator[] _HOCL_atomIteratorArray14;
																		_HOCL_atomIteratorArray14 = new AtomIterator[1];
																		{
																			class AtomIterator_out extends IteratorForExternal {
																				public AtomIterator_out(){
																					access = Access.REWRITE;
																				}
																				@Override
																				public boolean conditionSatisfied() {
																					Atom atom;
																					boolean satisfied;
																					atom = iterator.getElement();
																					satisfied = false;
																					if (atom instanceof ExternalObject
																					  && ((ExternalObject)atom).getObject() instanceof String) {
																						satisfied = true;
																					}
																					return satisfied;
																				}
																			
																			} // end of class AtomIterator_out
																			_HOCL_atomIteratorArray14[0] = new AtomIterator_out();
																		}
																		MoleculeIterator[] _HOCL_moleculeIteratorArray14 = new MoleculeIterator[0];
																		perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray14, _HOCL_moleculeIteratorArray14);
																		return perm;
																	}
																
																} // class IteratorSolution25
																_HOCL_atomIteratorArrayTuple15[1] = new IteratorSolution25();
															}
															_HOCL_atomIteratorArray14[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple15, Gw_pass.this);
														}
														{
															AtomIterator[] _HOCL_atomIteratorArrayTuple16;
															_HOCL_atomIteratorArrayTuple16 = new AtomIterator[2];
															{
																class AtomIterator__HOCL_literal14 extends IteratorForExternal {
																	public AtomIterator__HOCL_literal14(){
																		access = Access.REWRITE;
																	}
																	@Override
																	public boolean conditionSatisfied() {
																		Atom atom;
																		boolean satisfied;
																		atom = iterator.getElement();
																		satisfied = false;
																		if (atom instanceof ExternalObject
																		  && ((ExternalObject)atom).getObject() instanceof String) {
																			
																			IteratorForTuple _HOCL_tupleAtomIterator30 = (IteratorForTuple)permutation.getAtomIterator(1);
																			IteratorForSolution _HOCL_iteratorSolution15 = (IteratorForSolution)_HOCL_tupleAtomIterator30.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator31 = (IteratorForTuple)_HOCL_iteratorSolution15.getSubPermutation().getAtomIterator(0);
																			IteratorForSolution _HOCL_iteratorSolution16 = (IteratorForSolution)_HOCL_tupleAtomIterator31.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator32 = (IteratorForTuple)_HOCL_iteratorSolution16.getSubPermutation().getAtomIterator(0);
																			IteratorForSolution _HOCL_iteratorSolution17 = (IteratorForSolution)_HOCL_tupleAtomIterator32.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator33 = (IteratorForTuple)_HOCL_iteratorSolution17.getSubPermutation().getAtomIterator(1);
																			String _HOCL_literal14 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator33.getAtomIterator(0)).getObject();
																			satisfied = _HOCL_literal14.equals("EXIT");
																		}
																		return satisfied;
																	}
																
																} // end of class AtomIterator__HOCL_literal14
																_HOCL_atomIteratorArrayTuple16[0] = new AtomIterator__HOCL_literal14();
															}
															{
																class IteratorSolution27 extends IteratorForSolution {
																	protected Permutation makeSubPermutation(){
																		Permutation perm;
																		AtomIterator[] _HOCL_atomIteratorArray15;
																		_HOCL_atomIteratorArray15 = new AtomIterator[1];
																		{
																			class AtomIterator_exit extends IteratorForExternal {
																				public AtomIterator_exit(){
																					access = Access.REWRITE;
																				}
																				@Override
																				public boolean conditionSatisfied() {
																					Atom atom;
																					boolean satisfied;
																					atom = iterator.getElement();
																					satisfied = false;
																					if (atom instanceof ExternalObject
																					  && ((ExternalObject)atom).getObject() instanceof Integer) {
																						satisfied = true;
																					}
																					return satisfied;
																				}
																			
																			} // end of class AtomIterator_exit
																			_HOCL_atomIteratorArray15[0] = new AtomIterator_exit();
																		}
																		MoleculeIterator[] _HOCL_moleculeIteratorArray15 = new MoleculeIterator[0];
																		perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray15, _HOCL_moleculeIteratorArray15);
																		return perm;
																	}
																
																} // class IteratorSolution27
																_HOCL_atomIteratorArrayTuple16[1] = new IteratorSolution27();
															}
															_HOCL_atomIteratorArray14[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple16, Gw_pass.this);
														}
														MoleculeIterator[] _HOCL_moleculeIteratorArray16 = new MoleculeIterator[1];
														_HOCL_moleculeIteratorArray16[0] = new MoleculeIterator(1); // w_res
														
														perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray14, _HOCL_moleculeIteratorArray16);
														return perm;
													}
												
												} // class IteratorSolution29
												_HOCL_atomIteratorArrayTuple14[1] = new IteratorSolution29();
											}
											_HOCL_atomIteratorArray14[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple14, Gw_pass.this);
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray17 = new MoleculeIterator[0];
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray14, _HOCL_moleculeIteratorArray17);
										return perm;
									}
								
								} // class IteratorSolution31
								_HOCL_atomIteratorArrayTuple13[1] = new IteratorSolution31();
							}
							_HOCL_atomIteratorArray14[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple13, Gw_pass.this);
						}
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple17;
							_HOCL_atomIteratorArrayTuple17 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal15 extends IteratorForExternal {
									public AtomIterator__HOCL_literal15(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator34 = (IteratorForTuple)permutation.getAtomIterator(1);
											IteratorForSolution _HOCL_iteratorSolution18 = (IteratorForSolution)_HOCL_tupleAtomIterator34.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator35 = (IteratorForTuple)_HOCL_iteratorSolution18.getSubPermutation().getAtomIterator(1);
											String _HOCL_literal15 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator35.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal15.equals("DST");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal15
								_HOCL_atomIteratorArrayTuple17[0] = new AtomIterator__HOCL_literal15();
							}
							{
								class IteratorSolution33 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray18;
										_HOCL_atomIteratorArray18 = new AtomIterator[1];
										{
											class AtomIterator__HOCL_t_j_copy0 extends IteratorForExternal {
												public AtomIterator__HOCL_t_j_copy0(){
													access = Access.REWRITE;
												}
												@Override
												public boolean conditionSatisfied() {
													Atom atom;
													boolean satisfied;
													atom = iterator.getElement();
													satisfied = false;
													if (atom instanceof ExternalObject
													  && ((ExternalObject)atom).getObject() instanceof String) {
														
														IteratorForTuple _HOCL_tupleAtomIterator36 = (IteratorForTuple)permutation.getAtomIterator(0);
														String t_j = (String)((IteratorForExternal)_HOCL_tupleAtomIterator36.getAtomIterator(0)).getObject();
														IteratorForTuple _HOCL_tupleAtomIterator37 = (IteratorForTuple)permutation.getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution19 = (IteratorForSolution)_HOCL_tupleAtomIterator37.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator38 = (IteratorForTuple)_HOCL_iteratorSolution19.getSubPermutation().getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution20 = (IteratorForSolution)_HOCL_tupleAtomIterator38.getAtomIterator(1);
														String _HOCL_t_j_copy0 = (String)((IteratorForExternal)_HOCL_iteratorSolution20.getSubPermutation().getAtomIterator(0)).getObject();
														IteratorForTuple _HOCL_tupleAtomIterator39 = (IteratorForTuple)permutation.getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution21 = (IteratorForSolution)_HOCL_tupleAtomIterator39.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator40 = (IteratorForTuple)_HOCL_iteratorSolution21.getSubPermutation().getAtomIterator(0);
														IteratorForSolution _HOCL_iteratorSolution22 = (IteratorForSolution)_HOCL_tupleAtomIterator40.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator41 = (IteratorForTuple)_HOCL_iteratorSolution22.getSubPermutation().getAtomIterator(0);
														IteratorForSolution _HOCL_iteratorSolution23 = (IteratorForSolution)_HOCL_tupleAtomIterator41.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator42 = (IteratorForTuple)_HOCL_iteratorSolution23.getSubPermutation().getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution24 = (IteratorForSolution)_HOCL_tupleAtomIterator42.getAtomIterator(1);
														Integer exit = (Integer)((IteratorForExternal)_HOCL_iteratorSolution24.getSubPermutation().getAtomIterator(0)).getObject();
														satisfied = _HOCL_t_j_copy0.equals(t_j) && (exit == 0);
													}
													return satisfied;
												}
											
											} // end of class AtomIterator__HOCL_t_j_copy0
											_HOCL_atomIteratorArray18[0] = new AtomIterator__HOCL_t_j_copy0();
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray18 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray18[0] = new MoleculeIterator(1); // w_dst
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray18, _HOCL_moleculeIteratorArray18);
										return perm;
									}
								
								} // class IteratorSolution33
								_HOCL_atomIteratorArrayTuple17[1] = new IteratorSolution33();
							}
							_HOCL_atomIteratorArray14[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple17, Gw_pass.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray19 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray19[0] = new MoleculeIterator(1); // w_i
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray14, _HOCL_moleculeIteratorArray19);
						return perm;
					}
				
				} // class IteratorSolution35
				_HOCL_atomIteratorArrayTuple12[1] = new IteratorSolution35();
			}
			_HOCL_atomIteratorArray11[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple12, Gw_pass.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray20 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray11, _HOCL_moleculeIteratorArray20);
	}
	public Gw_pass clone() {
		 return new Gw_pass();
	}

	public void addType(String s){}

	// compute result of the rule gw_pass
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator43 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution25 = (IteratorForSolution)_HOCL_tupleAtomIterator43.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator44 = (IteratorForTuple)_HOCL_iteratorSolution25.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution26 = (IteratorForSolution)_HOCL_tupleAtomIterator44.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator45 = (IteratorForTuple)_HOCL_iteratorSolution26.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution27 = (IteratorForSolution)_HOCL_tupleAtomIterator45.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator46 = (IteratorForTuple)_HOCL_iteratorSolution27.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution28 = (IteratorForSolution)_HOCL_tupleAtomIterator46.getAtomIterator(1);
		String out = (String)((IteratorForExternal)_HOCL_iteratorSolution28.getSubPermutation().getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator47 = (IteratorForTuple)permutation.getAtomIterator(0);
		String t_j = (String)((IteratorForExternal)_HOCL_tupleAtomIterator47.getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator48 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution29 = (IteratorForSolution)_HOCL_tupleAtomIterator48.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator49 = (IteratorForTuple)_HOCL_iteratorSolution29.getSubPermutation().getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution30 = (IteratorForSolution)_HOCL_tupleAtomIterator49.getAtomIterator(1);
		Molecule w_in = _HOCL_iteratorSolution30.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator50 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution31 = (IteratorForSolution)_HOCL_tupleAtomIterator50.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator51 = (IteratorForTuple)_HOCL_iteratorSolution31.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution32 = (IteratorForSolution)_HOCL_tupleAtomIterator51.getAtomIterator(1);
		String t_i = (String)((IteratorForExternal)_HOCL_iteratorSolution32.getSubPermutation().getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator52 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution33 = (IteratorForSolution)_HOCL_tupleAtomIterator52.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator53 = (IteratorForTuple)_HOCL_iteratorSolution33.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution34 = (IteratorForSolution)_HOCL_tupleAtomIterator53.getAtomIterator(1);
		Molecule w_src = _HOCL_iteratorSolution34.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator54 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution35 = (IteratorForSolution)_HOCL_tupleAtomIterator54.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator55 = (IteratorForTuple)_HOCL_iteratorSolution35.getSubPermutation().getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution36 = (IteratorForSolution)_HOCL_tupleAtomIterator55.getAtomIterator(1);
		Molecule w_dst = _HOCL_iteratorSolution36.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator56 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution37 = (IteratorForSolution)_HOCL_tupleAtomIterator56.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator57 = (IteratorForTuple)_HOCL_iteratorSolution37.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution38 = (IteratorForSolution)_HOCL_tupleAtomIterator57.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator58 = (IteratorForTuple)_HOCL_iteratorSolution38.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution39 = (IteratorForSolution)_HOCL_tupleAtomIterator58.getAtomIterator(1);
		Molecule w_res = _HOCL_iteratorSolution39.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator59 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution40 = (IteratorForSolution)_HOCL_tupleAtomIterator59.getAtomIterator(1);
		Molecule w_i = _HOCL_iteratorSolution40.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator60 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution41 = (IteratorForSolution)_HOCL_tupleAtomIterator60.getAtomIterator(1);
		Molecule w_j = _HOCL_iteratorSolution41.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator61 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution42 = (IteratorForSolution)_HOCL_tupleAtomIterator61.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator62 = (IteratorForTuple)_HOCL_iteratorSolution42.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution43 = (IteratorForSolution)_HOCL_tupleAtomIterator62.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator63 = (IteratorForTuple)_HOCL_iteratorSolution43.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution44 = (IteratorForSolution)_HOCL_tupleAtomIterator63.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator64 = (IteratorForTuple)_HOCL_iteratorSolution44.getSubPermutation().getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution45 = (IteratorForSolution)_HOCL_tupleAtomIterator64.getAtomIterator(1);
		Integer exit = (Integer)((IteratorForExternal)_HOCL_iteratorSolution45.getSubPermutation().getAtomIterator(0)).getObject();
		// new Molecule  
		Molecule mol11 = new Molecule();
		Tuple tuple10 = new Tuple(2);
		tuple10.set(0, ExternalObject.getHOCL_TypeTranslation(t_i));
		// new solution + 0 
		Solution solution14 = new Solution(); 
		{
			// new Molecule  
			Molecule mol12 = new Molecule();
			Tuple tuple11 = new Tuple(2);
			tuple11.set(0, ExternalObject.getHOCL_TypeTranslation("RES"));
			// new solution + 1 
			Solution solution12 = new Solution(); 
			{
				// new Molecule  
				Molecule mol13 = new Molecule();
				Tuple tuple12 = new Tuple(2);
				tuple12.set(0, ExternalObject.getHOCL_TypeTranslation("TRANSFER"));
				// new solution + 2 
				Solution solution11 = new Solution(); 
				{
					// new Molecule  
					Molecule mol14 = new Molecule();
					Tuple tuple13 = new Tuple(2);
					tuple13.set(0, ExternalObject.getHOCL_TypeTranslation("OUT"));
					// new solution + 3 
					Solution solution9 = new Solution(); 
					{
						// new Molecule  
						Molecule mol15 = new Molecule();
						{ 
						Atom atomReference = ExternalObject.getHOCL_TypeTranslation(out);
						if (atomReference instanceof ExternalObject) {
							ExternalObject auxObject = (ExternalObject) atomReference;
							mol15.add(auxObject);
							string = auxObject.getObject().getClass().toString().split("\\.");
							this.addType(string[string.length-1]);
						} else {
							mol15.add(atomReference);
						if(atomReference instanceof Tuple) 
							this.addType("Tuple");
						}
						} 
						
						solution9.addMolecule(mol15);
					}
					tuple13.set(1, solution9);
					tuple = tuple13;
					mol14.add(tuple);
					this.addType("Tuple");
					
					
					Tuple tuple14 = new Tuple(2);
					tuple14.set(0, ExternalObject.getHOCL_TypeTranslation("EXIT"));
					// new solution + 3 
					Solution solution10 = new Solution(); 
					{
						// new Molecule  
						Molecule mol16 = new Molecule();
						{ 
						Atom atomReference = ExternalObject.getHOCL_TypeTranslation(exit);
						if (atomReference instanceof ExternalObject) {
							ExternalObject auxObject = (ExternalObject) atomReference;
							mol16.add(auxObject);
							string = auxObject.getObject().getClass().toString().split("\\.");
							this.addType(string[string.length-1]);
						} else {
							mol16.add(atomReference);
						if(atomReference instanceof Tuple) 
							this.addType("Tuple");
						}
						} 
						
						solution10.addMolecule(mol16);
					}
					tuple14.set(1, solution10);
					tuple = tuple14;
					mol14.add(tuple);
					this.addType("Tuple");
					
					
					mol14.add(w_res);
					solution11.addMolecule(mol14);
				}
				tuple12.set(1, solution11);
				tuple = tuple12;
				mol13.add(tuple);
				this.addType("Tuple");
				
				
				solution12.addMolecule(mol13);
			}
			tuple11.set(1, solution12);
			tuple = tuple11;
			mol12.add(tuple);
			this.addType("Tuple");
			
			
			Tuple tuple15 = new Tuple(2);
			tuple15.set(0, ExternalObject.getHOCL_TypeTranslation("DST"));
			// new solution + 1 
			Solution solution13 = new Solution(); 
			{
				// new Molecule  
				Molecule mol17 = new Molecule();
				mol17.add(w_dst);
				solution13.addMolecule(mol17);
			}
			tuple15.set(1, solution13);
			tuple = tuple15;
			mol12.add(tuple);
			this.addType("Tuple");
			
			
			mol12.add(w_i);
			solution14.addMolecule(mol12);
		}
		tuple10.set(1, solution14);
		tuple = tuple10;
		mol11.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple16 = new Tuple(2);
		tuple16.set(0, ExternalObject.getHOCL_TypeTranslation(t_j));
		// new solution + 0 
		Solution solution17 = new Solution(); 
		{
			// new Molecule  
			Molecule mol18 = new Molecule();
			Tuple tuple17 = new Tuple(2);
			tuple17.set(0, ExternalObject.getHOCL_TypeTranslation("SRC"));
			// new solution + 1 
			Solution solution15 = new Solution(); 
			{
				// new Molecule  
				Molecule mol19 = new Molecule();
				mol19.add(w_src);
				solution15.addMolecule(mol19);
			}
			tuple17.set(1, solution15);
			tuple = tuple17;
			mol18.add(tuple);
			this.addType("Tuple");
			
			
			Tuple tuple18 = new Tuple(2);
			tuple18.set(0, ExternalObject.getHOCL_TypeTranslation("IN"));
			// new solution + 1 
			Solution solution16 = new Solution(); 
			{
				// new Molecule  
				Molecule mol20 = new Molecule();
				{ 
				Atom atomReference = ExternalObject.getHOCL_TypeTranslation(out);
				if (atomReference instanceof ExternalObject) {
					ExternalObject auxObject = (ExternalObject) atomReference;
					mol20.add(auxObject);
					string = auxObject.getObject().getClass().toString().split("\\.");
					this.addType(string[string.length-1]);
				} else {
					mol20.add(atomReference);
				if(atomReference instanceof Tuple) 
					this.addType("Tuple");
				}
				} 
				
				mol20.add(w_in);
				solution16.addMolecule(mol20);
			}
			tuple18.set(1, solution16);
			tuple = tuple18;
			mol18.add(tuple);
			this.addType("Tuple");
			
			
			mol18.add(w_j);
			solution17.addMolecule(mol18);
		}
		tuple16.set(1, solution17);
		tuple = tuple16;
		mol11.add(tuple);
		this.addType("Tuple");
		
		
		return mol11;
	}

} // end of class Gw_pass
