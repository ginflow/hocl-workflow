/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Wa_trigger_src extends ReactionRule implements Serializable {

	
	public Wa_trigger_src(){
		super("wa_trigger_src", Shot.ONE_SHOT);
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray33;
		_HOCL_atomIteratorArray33 = new AtomIterator[1];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple28;
			_HOCL_atomIteratorArrayTuple28 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal24 extends IteratorForExternal {
					public AtomIterator__HOCL_literal24(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator116 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal24 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator116.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal24.equals("T_1");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal24
				_HOCL_atomIteratorArrayTuple28[0] = new AtomIterator__HOCL_literal24();
			}
			{
				class IteratorSolution59 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray33;
						_HOCL_atomIteratorArray33 = new AtomIterator[1];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple29;
							_HOCL_atomIteratorArrayTuple29 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal25 extends IteratorForExternal {
									public AtomIterator__HOCL_literal25(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator117 = (IteratorForTuple)permutation.getAtomIterator(0);
											IteratorForSolution _HOCL_iteratorSolution86 = (IteratorForSolution)_HOCL_tupleAtomIterator117.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator118 = (IteratorForTuple)_HOCL_iteratorSolution86.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal25 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator118.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal25.equals("DST");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal25
								_HOCL_atomIteratorArrayTuple29[0] = new AtomIterator__HOCL_literal25();
							}
							{
								class IteratorSolution57 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray33;
										_HOCL_atomIteratorArray33 = new AtomIterator[0];
										
										MoleculeIterator[] _HOCL_moleculeIteratorArray33 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray33[0] = new MoleculeIterator(1); // w_dst
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray33, _HOCL_moleculeIteratorArray33);
										return perm;
									}
								
								} // class IteratorSolution57
								_HOCL_atomIteratorArrayTuple29[1] = new IteratorSolution57();
							}
							_HOCL_atomIteratorArray33[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple29, Wa_trigger_src.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray34 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray34[0] = new MoleculeIterator(1); // w_1
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray33, _HOCL_moleculeIteratorArray34);
						return perm;
					}
				
				} // class IteratorSolution59
				_HOCL_atomIteratorArrayTuple28[1] = new IteratorSolution59();
			}
			_HOCL_atomIteratorArray33[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple28, Wa_trigger_src.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray35 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray33, _HOCL_moleculeIteratorArray35);
	}
	public Wa_trigger_src clone() {
		 return new Wa_trigger_src();
	}

	public void addType(String s){}

	// compute result of the rule wa_trigger_src
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator119 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution87 = (IteratorForSolution)_HOCL_tupleAtomIterator119.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator120 = (IteratorForTuple)_HOCL_iteratorSolution87.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution88 = (IteratorForSolution)_HOCL_tupleAtomIterator120.getAtomIterator(1);
		Molecule w_dst = _HOCL_iteratorSolution88.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator121 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution89 = (IteratorForSolution)_HOCL_tupleAtomIterator121.getAtomIterator(1);
		Molecule w_1 = _HOCL_iteratorSolution89.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol33 = new Molecule();
		Tuple tuple29 = new Tuple(2);
		tuple29.set(0, ExternalObject.getHOCL_TypeTranslation("T_1"));
		// new solution + 0 
		Solution solution29 = new Solution(); 
		{
			// new Molecule  
			Molecule mol34 = new Molecule();
			Tuple tuple30 = new Tuple(2);
			tuple30.set(0, ExternalObject.getHOCL_TypeTranslation("DST"));
			// new solution + 1 
			Solution solution28 = new Solution(); 
			{
				// new Molecule  
				Molecule mol35 = new Molecule();
				{ 
				Atom atomReference = ExternalObject.getHOCL_TypeTranslation("T_4");
				if (atomReference instanceof ExternalObject) {
					ExternalObject auxObject = (ExternalObject) atomReference;
					mol35.add(auxObject);
					string = auxObject.getObject().getClass().toString().split("\\.");
					this.addType(string[string.length-1]);
				} else {
					mol35.add(atomReference);
				if(atomReference instanceof Tuple) 
					this.addType("Tuple");
				}
				} 
				
				{ 
				Atom atomReference = ExternalObject.getHOCL_TypeTranslation("T_44");
				if (atomReference instanceof ExternalObject) {
					ExternalObject auxObject = (ExternalObject) atomReference;
					mol35.add(auxObject);
					string = auxObject.getObject().getClass().toString().split("\\.");
					this.addType(string[string.length-1]);
				} else {
					mol35.add(atomReference);
				if(atomReference instanceof Tuple) 
					this.addType("Tuple");
				}
				} 
				
				mol35.add(w_dst);
				solution28.addMolecule(mol35);
			}
			tuple30.set(1, solution28);
			tuple = tuple30;
			mol34.add(tuple);
			this.addType("Tuple");
			
			
			mol34.add(w_1);
			solution29.addMolecule(mol34);
		}
		tuple29.set(1, solution29);
		tuple = tuple29;
		mol33.add(tuple);
		this.addType("Tuple");
		
		
		return mol33;
	}

} // end of class Wa_trigger_src
