/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Supervise extends ReactionRule implements Serializable {

	
	public Supervise(){
		super("supervise", Shot.ONE_SHOT);
		setTrope(Trope.EXPANSER);
			AtomIterator[] _HOCL_atomIteratorArray36;
		_HOCL_atomIteratorArray36 = new AtomIterator[2];
		{
			class AtomIterator__HOCL_literal26 extends IteratorForExternal {
				public AtomIterator__HOCL_literal26(){
					access = Access.REWRITE;
				}
				@Override
				public boolean conditionSatisfied() {
					Atom atom;
					boolean satisfied;
					atom = iterator.getElement();
					satisfied = false;
					if (atom instanceof ExternalObject
					  && ((ExternalObject)atom).getObject() instanceof String) {
						
						String _HOCL_literal26 = (String)((IteratorForExternal)permutation.getAtomIterator(0)).getObject();
						satisfied = _HOCL_literal26.equals("ADAPT");
					}
					return satisfied;
				}
			
			} // end of class AtomIterator__HOCL_literal26
			_HOCL_atomIteratorArray36[0] = new AtomIterator__HOCL_literal26();
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple30;
			_HOCL_atomIteratorArrayTuple30 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal27 extends IteratorForExternal {
					public AtomIterator__HOCL_literal27(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator122 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal27 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator122.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal27.equals("T_2");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal27
				_HOCL_atomIteratorArrayTuple30[0] = new AtomIterator__HOCL_literal27();
			}
			{
				class IteratorSolution65 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray36;
						_HOCL_atomIteratorArray36 = new AtomIterator[1];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple31;
							_HOCL_atomIteratorArrayTuple31 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal28 extends IteratorForExternal {
									public AtomIterator__HOCL_literal28(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator123 = (IteratorForTuple)permutation.getAtomIterator(1);
											IteratorForSolution _HOCL_iteratorSolution90 = (IteratorForSolution)_HOCL_tupleAtomIterator123.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator124 = (IteratorForTuple)_HOCL_iteratorSolution90.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal28 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator124.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal28.equals("RES");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal28
								_HOCL_atomIteratorArrayTuple31[0] = new AtomIterator__HOCL_literal28();
							}
							{
								class IteratorSolution63 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray36;
										_HOCL_atomIteratorArray36 = new AtomIterator[1];
										{
											AtomIterator[] _HOCL_atomIteratorArrayTuple32;
											_HOCL_atomIteratorArrayTuple32 = new AtomIterator[2];
											{
												class AtomIterator__HOCL_literal29 extends IteratorForExternal {
													public AtomIterator__HOCL_literal29(){
														access = Access.REWRITE;
													}
													@Override
													public boolean conditionSatisfied() {
														Atom atom;
														boolean satisfied;
														atom = iterator.getElement();
														satisfied = false;
														if (atom instanceof ExternalObject
														  && ((ExternalObject)atom).getObject() instanceof String) {
															
															IteratorForTuple _HOCL_tupleAtomIterator125 = (IteratorForTuple)permutation.getAtomIterator(1);
															IteratorForSolution _HOCL_iteratorSolution91 = (IteratorForSolution)_HOCL_tupleAtomIterator125.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator126 = (IteratorForTuple)_HOCL_iteratorSolution91.getSubPermutation().getAtomIterator(0);
															IteratorForSolution _HOCL_iteratorSolution92 = (IteratorForSolution)_HOCL_tupleAtomIterator126.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator127 = (IteratorForTuple)_HOCL_iteratorSolution92.getSubPermutation().getAtomIterator(0);
															String _HOCL_literal29 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator127.getAtomIterator(0)).getObject();
															satisfied = _HOCL_literal29.equals("EXIT");
														}
														return satisfied;
													}
												
												} // end of class AtomIterator__HOCL_literal29
												_HOCL_atomIteratorArrayTuple32[0] = new AtomIterator__HOCL_literal29();
											}
											{
												class IteratorSolution61 extends IteratorForSolution {
													protected Permutation makeSubPermutation(){
														Permutation perm;
														AtomIterator[] _HOCL_atomIteratorArray36;
														_HOCL_atomIteratorArray36 = new AtomIterator[1];
														{
															class AtomIterator_exit extends IteratorForExternal {
																public AtomIterator_exit(){
																	access = Access.REWRITE;
																}
																@Override
																public boolean conditionSatisfied() {
																	Atom atom;
																	boolean satisfied;
																	atom = iterator.getElement();
																	satisfied = false;
																	if (atom instanceof ExternalObject
																	  && ((ExternalObject)atom).getObject() instanceof Integer) {
																		
																		IteratorForTuple _HOCL_tupleAtomIterator128 = (IteratorForTuple)permutation.getAtomIterator(1);
																		IteratorForSolution _HOCL_iteratorSolution93 = (IteratorForSolution)_HOCL_tupleAtomIterator128.getAtomIterator(1);
																		IteratorForTuple _HOCL_tupleAtomIterator129 = (IteratorForTuple)_HOCL_iteratorSolution93.getSubPermutation().getAtomIterator(0);
																		IteratorForSolution _HOCL_iteratorSolution94 = (IteratorForSolution)_HOCL_tupleAtomIterator129.getAtomIterator(1);
																		IteratorForTuple _HOCL_tupleAtomIterator130 = (IteratorForTuple)_HOCL_iteratorSolution94.getSubPermutation().getAtomIterator(0);
																		IteratorForSolution _HOCL_iteratorSolution95 = (IteratorForSolution)_HOCL_tupleAtomIterator130.getAtomIterator(1);
																		Integer exit = (Integer)((IteratorForExternal)_HOCL_iteratorSolution95.getSubPermutation().getAtomIterator(0)).getObject();
																		satisfied = (exit == -1);
																	}
																	return satisfied;
																}
															
															} // end of class AtomIterator_exit
															_HOCL_atomIteratorArray36[0] = new AtomIterator_exit();
														}
														MoleculeIterator[] _HOCL_moleculeIteratorArray36 = new MoleculeIterator[0];
														perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray36, _HOCL_moleculeIteratorArray36);
														return perm;
													}
												
												} // class IteratorSolution61
												_HOCL_atomIteratorArrayTuple32[1] = new IteratorSolution61();
											}
											_HOCL_atomIteratorArray36[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple32, Supervise.this);
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray37 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray37[0] = new MoleculeIterator(1); // w_res
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray36, _HOCL_moleculeIteratorArray37);
										return perm;
									}
								
								} // class IteratorSolution63
								_HOCL_atomIteratorArrayTuple31[1] = new IteratorSolution63();
							}
							_HOCL_atomIteratorArray36[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple31, Supervise.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray38 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray38[0] = new MoleculeIterator(1); // w2
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray36, _HOCL_moleculeIteratorArray38);
						return perm;
					}
				
				} // class IteratorSolution65
				_HOCL_atomIteratorArrayTuple30[1] = new IteratorSolution65();
			}
			_HOCL_atomIteratorArray36[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple30, Supervise.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray39 = new MoleculeIterator[1];
		_HOCL_moleculeIteratorArray39[0] = new MoleculeIterator(0);
		
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray36, _HOCL_moleculeIteratorArray39);
	}
	public Supervise clone() {
		 return new Supervise();
	}

	public void addType(String s){}

	// compute result of the rule supervise
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		Molecule w = permutation.getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator131 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution96 = (IteratorForSolution)_HOCL_tupleAtomIterator131.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator132 = (IteratorForTuple)_HOCL_iteratorSolution96.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution97 = (IteratorForSolution)_HOCL_tupleAtomIterator132.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator133 = (IteratorForTuple)_HOCL_iteratorSolution97.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution98 = (IteratorForSolution)_HOCL_tupleAtomIterator133.getAtomIterator(1);
		Integer exit = (Integer)((IteratorForExternal)_HOCL_iteratorSolution98.getSubPermutation().getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator134 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution99 = (IteratorForSolution)_HOCL_tupleAtomIterator134.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator135 = (IteratorForTuple)_HOCL_iteratorSolution99.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution100 = (IteratorForSolution)_HOCL_tupleAtomIterator135.getAtomIterator(1);
		Molecule w_res = _HOCL_iteratorSolution100.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator136 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution101 = (IteratorForSolution)_HOCL_tupleAtomIterator136.getAtomIterator(1);
		Molecule w2 = _HOCL_iteratorSolution101.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol36 = new Molecule();
		Tuple tuple31 = new Tuple(2);
		tuple31.set(0, ExternalObject.getHOCL_TypeTranslation("T_2"));
		// new solution + 0 
		Solution solution32 = new Solution(); 
		{
			// new Molecule  
			Molecule mol37 = new Molecule();
			Tuple tuple32 = new Tuple(2);
			tuple32.set(0, ExternalObject.getHOCL_TypeTranslation("RES"));
			// new solution + 1 
			Solution solution31 = new Solution(); 
			{
				// new Molecule  
				Molecule mol38 = new Molecule();
				Tuple tuple33 = new Tuple(2);
				tuple33.set(0, ExternalObject.getHOCL_TypeTranslation("EXIT"));
				// new solution + 2 
				Solution solution30 = new Solution(); 
				{
					// new Molecule  
					Molecule mol39 = new Molecule();
					{ 
					Atom atomReference = ExternalObject.getHOCL_TypeTranslation(exit);
					if (atomReference instanceof ExternalObject) {
						ExternalObject auxObject = (ExternalObject) atomReference;
						mol39.add(auxObject);
						string = auxObject.getObject().getClass().toString().split("\\.");
						this.addType(string[string.length-1]);
					} else {
						mol39.add(atomReference);
					if(atomReference instanceof Tuple) 
						this.addType("Tuple");
					}
					} 
					
					solution30.addMolecule(mol39);
				}
				tuple33.set(1, solution30);
				tuple = tuple33;
				mol38.add(tuple);
				this.addType("Tuple");
				
				
				mol38.add(w_res);
				solution31.addMolecule(mol38);
			}
			tuple32.set(1, solution31);
			tuple = tuple32;
			mol37.add(tuple);
			this.addType("Tuple");
			
			
			mol37.add(w2);
			solution32.addMolecule(mol37);
		}
		tuple31.set(1, solution32);
		tuple = tuple31;
		mol36.add(tuple);
		this.addType("Tuple");
		
		
		rule = new Wa_trigger_dst();
		mol36.add(rule);
		this.addType(rule.getName());
		
		
		rule = new Wa_trigger_src();
		mol36.add(rule);
		this.addType(rule.getName());
		
		
		mol36.add(w);
		return mol36;
	}

} // end of class Supervise
