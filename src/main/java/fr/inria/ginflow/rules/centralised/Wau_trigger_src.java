/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.hocl.core.hocli.*;

import java.io.*;

public class Wau_trigger_src extends ReactionRule implements Serializable {

    /** The service to update.*/
    private String service_;
    
    /** The faulty service.*/
    private String faulty_;
    
    /** The alternative.*/
    private String alternative_;
    
	public Wau_trigger_src(String service, String faulty, String alternative){
		super("wa_trigger_src", Shot.ONE_SHOT);
		
		service_ = service;
        faulty_ = faulty;
        alternative_ = alternative;
		
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray20;
		_HOCL_atomIteratorArray20 = new AtomIterator[1];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple14;
			_HOCL_atomIteratorArrayTuple14 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal13 extends IteratorForExternal {
					public AtomIterator__HOCL_literal13(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator41 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal13 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator41.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal13.equals(service_);
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal13
				_HOCL_atomIteratorArrayTuple14[0] = new AtomIterator__HOCL_literal13();
			}
			{
				class IteratorSolution29 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray20;
						_HOCL_atomIteratorArray20 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray20 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray20[0] = new MoleculeIterator(1); // w_1
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray20, _HOCL_moleculeIteratorArray20);
						return perm;
					}
				
				} // class IteratorSolution29
				_HOCL_atomIteratorArrayTuple14[1] = new IteratorSolution29();
			}
			_HOCL_atomIteratorArray20[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple14, Wau_trigger_src.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray21 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray20, _HOCL_moleculeIteratorArray21);
	}
	public Wau_trigger_src clone() {
		 return new Wau_trigger_src(service_, faulty_, alternative_);
	}

	public void addType(String s){}

	// compute result of the rule wa_trigger_src
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator42 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution26 = (IteratorForSolution)_HOCL_tupleAtomIterator42.getAtomIterator(1);
		Molecule w_1 = _HOCL_iteratorSolution26.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol20 = new Molecule();
		Tuple tuple14 = new Tuple(2);
		tuple14.set(0, ExternalObject.getHOCL_TypeTranslation(service_));
		// new solution + 0 
		Solution solution14 = new Solution(); 
		{
			// new Molecule  
			Molecule mol21 = new Molecule();
			rule = new Wau_update_dst(faulty_, alternative_);
			mol21.add(rule);
			this.addType(rule.getName());
			
			
			mol21.add(w_1);
			solution14.addMolecule(mol21);
		}
		tuple14.set(1, solution14);
		tuple = tuple14;
		mol20.add(tuple);
		this.addType("Tuple");
		
		
		return mol20;
	}

} // end of class Wa_trigger_src
