/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_send_control extends ReactionRule implements Serializable {

	
	public Gw_send_control(){
		super("gw_send_control", Shot.N_SHOT);
		setTrope(Trope.EXPANSER);
			AtomIterator[] _HOCL_atomIteratorArray26;
		_HOCL_atomIteratorArray26 = new AtomIterator[2];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple20;
			_HOCL_atomIteratorArrayTuple20 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal20 extends IteratorForExternal {
					public AtomIterator__HOCL_literal20(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator56 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal20 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator56.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal20.equals("DST_CONTROL");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal20
				_HOCL_atomIteratorArrayTuple20[0] = new AtomIterator__HOCL_literal20();
			}
			{
				class IteratorSolution41 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray26;
						_HOCL_atomIteratorArray26 = new AtomIterator[1];
						{
							class AtomIterator_d extends IteratorForExternal {
								public AtomIterator_d(){
									access = Access.REWRITE;
								}
								@Override
								public boolean conditionSatisfied() {
									Atom atom;
									boolean satisfied;
									atom = iterator.getElement();
									satisfied = false;
									if (atom instanceof ExternalObject
									  && ((ExternalObject)atom).getObject() instanceof String) {
										satisfied = true;
									}
									return satisfied;
								}
							
							} // end of class AtomIterator_d
							_HOCL_atomIteratorArray26[0] = new AtomIterator_d();
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray26 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray26[0] = new MoleculeIterator(1); // w_dst
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray26, _HOCL_moleculeIteratorArray26);
						return perm;
					}
				
				} // class IteratorSolution41
				_HOCL_atomIteratorArrayTuple20[1] = new IteratorSolution41();
			}
			_HOCL_atomIteratorArray26[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple20, Gw_send_control.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple21;
			_HOCL_atomIteratorArrayTuple21 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal21 extends IteratorForExternal {
					public AtomIterator__HOCL_literal21(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator57 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal21 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator57.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal21.equals("SEND");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal21
				_HOCL_atomIteratorArrayTuple21[0] = new AtomIterator__HOCL_literal21();
			}
			{
				class IteratorSolution43 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray27;
						_HOCL_atomIteratorArray27 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray27 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray27[0] = new MoleculeIterator(1); // w_res
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray27, _HOCL_moleculeIteratorArray27);
						return perm;
					}
				
				} // class IteratorSolution43
				_HOCL_atomIteratorArrayTuple21[1] = new IteratorSolution43();
			}
			_HOCL_atomIteratorArray26[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple21, Gw_send_control.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray28 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray26, _HOCL_moleculeIteratorArray28);
	}
	public Gw_send_control clone() {
		 return new Gw_send_control();
	}

	public void addType(String s){}

	// compute result of the rule gw_send_control
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator58 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution36 = (IteratorForSolution)_HOCL_tupleAtomIterator58.getAtomIterator(1);
		Molecule w_res = _HOCL_iteratorSolution36.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator59 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution37 = (IteratorForSolution)_HOCL_tupleAtomIterator59.getAtomIterator(1);
		Molecule w_dst = _HOCL_iteratorSolution37.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator60 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution38 = (IteratorForSolution)_HOCL_tupleAtomIterator60.getAtomIterator(1);
		String d = (String)((IteratorForExternal)_HOCL_iteratorSolution38.getSubPermutation().getAtomIterator(0)).getObject();
		// new Molecule  
		Molecule mol31 = new Molecule();
		Tuple tuple28 = new Tuple(2);
		tuple28.set(0, ExternalObject.getHOCL_TypeTranslation("TRF"));
		tuple28.set(1, ExternalObject.getHOCL_TypeTranslation(TransferMolecule.put(d, w_res)));
		tuple = tuple28;
		mol31.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple29 = new Tuple(2);
		tuple29.set(0, ExternalObject.getHOCL_TypeTranslation("DST_CONTROL"));
		// new solution + 0 
		Solution solution25 = new Solution(); 
		{
			// new Molecule  
			Molecule mol32 = new Molecule();
			mol32.add(w_dst);
			solution25.addMolecule(mol32);
		}
		tuple29.set(1, solution25);
		tuple = tuple29;
		mol31.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple30 = new Tuple(2);
		tuple30.set(0, ExternalObject.getHOCL_TypeTranslation("SEND"));
		// new solution + 0 
		Solution solution26 = new Solution(); 
		{
			// new Molecule  
			Molecule mol33 = new Molecule();
			mol33.add(w_res);
			solution26.addMolecule(mol33);
		}
		tuple30.set(1, solution26);
		tuple = tuple30;
		mol31.add(tuple);
		this.addType("Tuple");
		
		
		return mol31;
	}

} // end of class Gw_send_control
