/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_swap_src extends ReactionRule implements Serializable {

	
	public Gw_swap_src(){
		super("gw_swap_src", Shot.ONE_SHOT);
		setTrope(Trope.REDUCER);
			AtomIterator[] _HOCL_atomIteratorArray47;
		_HOCL_atomIteratorArray47 = new AtomIterator[6];
		{
			class AtomIterator__HOCL_literal38 extends IteratorForExternal {
				public AtomIterator__HOCL_literal38(){
					access = Access.REWRITE;
				}
				@Override
				public boolean conditionSatisfied() {
					Atom atom;
					boolean satisfied;
					atom = iterator.getElement();
					satisfied = false;
					if (atom instanceof ExternalObject
					  && ((ExternalObject)atom).getObject() instanceof String) {
						
						String _HOCL_literal38 = (String)((IteratorForExternal)permutation.getAtomIterator(0)).getObject();
						satisfied = _HOCL_literal38.equals("ADAPT");
					}
					return satisfied;
				}
			
			} // end of class AtomIterator__HOCL_literal38
			_HOCL_atomIteratorArray47[0] = new AtomIterator__HOCL_literal38();
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple37;
			_HOCL_atomIteratorArrayTuple37 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal39 extends IteratorForExternal {
					public AtomIterator__HOCL_literal39(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator95 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal39 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator95.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal39.equals("ADAPT_SRC");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal39
				_HOCL_atomIteratorArrayTuple37[0] = new AtomIterator__HOCL_literal39();
			}
			{
				class IteratorSolution75 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray47;
						_HOCL_atomIteratorArray47 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray47 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray47[0] = new MoleculeIterator(1); // w_adapt_src
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray47, _HOCL_moleculeIteratorArray47);
						return perm;
					}
				
				} // class IteratorSolution75
				_HOCL_atomIteratorArrayTuple37[1] = new IteratorSolution75();
			}
			_HOCL_atomIteratorArray47[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple37, Gw_swap_src.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple38;
			_HOCL_atomIteratorArrayTuple38 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal40 extends IteratorForExternal {
					public AtomIterator__HOCL_literal40(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator96 = (IteratorForTuple)permutation.getAtomIterator(2);
							String _HOCL_literal40 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator96.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal40.equals("SRC");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal40
				_HOCL_atomIteratorArrayTuple38[0] = new AtomIterator__HOCL_literal40();
			}
			{
				class IteratorSolution77 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray48;
						_HOCL_atomIteratorArray48 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray48 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray48[0] = new MoleculeIterator(1); // w_src
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray48, _HOCL_moleculeIteratorArray48);
						return perm;
					}
				
				} // class IteratorSolution77
				_HOCL_atomIteratorArrayTuple38[1] = new IteratorSolution77();
			}
			_HOCL_atomIteratorArray47[2] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple38, Gw_swap_src.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple39;
			_HOCL_atomIteratorArrayTuple39 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal41 extends IteratorForExternal {
					public AtomIterator__HOCL_literal41(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator97 = (IteratorForTuple)permutation.getAtomIterator(3);
							String _HOCL_literal41 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator97.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal41.equals("ADAPT_SRC_CONTROL");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal41
				_HOCL_atomIteratorArrayTuple39[0] = new AtomIterator__HOCL_literal41();
			}
			{
				class IteratorSolution79 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray49;
						_HOCL_atomIteratorArray49 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray49 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray49[0] = new MoleculeIterator(1); // w_adapt_src_control
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray49, _HOCL_moleculeIteratorArray49);
						return perm;
					}
				
				} // class IteratorSolution79
				_HOCL_atomIteratorArrayTuple39[1] = new IteratorSolution79();
			}
			_HOCL_atomIteratorArray47[3] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple39, Gw_swap_src.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple40;
			_HOCL_atomIteratorArrayTuple40 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal42 extends IteratorForExternal {
					public AtomIterator__HOCL_literal42(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator98 = (IteratorForTuple)permutation.getAtomIterator(4);
							String _HOCL_literal42 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator98.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal42.equals("SRC_CONTROL");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal42
				_HOCL_atomIteratorArrayTuple40[0] = new AtomIterator__HOCL_literal42();
			}
			{
				class IteratorSolution81 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray50;
						_HOCL_atomIteratorArray50 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray50 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray50[0] = new MoleculeIterator(1); // w_src_control
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray50, _HOCL_moleculeIteratorArray50);
						return perm;
					}
				
				} // class IteratorSolution81
				_HOCL_atomIteratorArrayTuple40[1] = new IteratorSolution81();
			}
			_HOCL_atomIteratorArray47[4] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple40, Gw_swap_src.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple41;
			_HOCL_atomIteratorArrayTuple41 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal43 extends IteratorForExternal {
					public AtomIterator__HOCL_literal43(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator99 = (IteratorForTuple)permutation.getAtomIterator(5);
							String _HOCL_literal43 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator99.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal43.equals("IN");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal43
				_HOCL_atomIteratorArrayTuple41[0] = new AtomIterator__HOCL_literal43();
			}
			{
				class IteratorSolution83 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray51;
						_HOCL_atomIteratorArray51 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray51 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray51[0] = new MoleculeIterator(1); // w_in
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray51, _HOCL_moleculeIteratorArray51);
						return perm;
					}
				
				} // class IteratorSolution83
				_HOCL_atomIteratorArrayTuple41[1] = new IteratorSolution83();
			}
			_HOCL_atomIteratorArray47[5] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple41, Gw_swap_src.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray52 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray47, _HOCL_moleculeIteratorArray52);
	}
	public Gw_swap_src clone() {
		 return new Gw_swap_src();
	}

	public void addType(String s){}

	// compute result of the rule gw_swap_src
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator100 = (IteratorForTuple)permutation.getAtomIterator(3);
		IteratorForSolution _HOCL_iteratorSolution58 = (IteratorForSolution)_HOCL_tupleAtomIterator100.getAtomIterator(1);
		Molecule w_adapt_src_control = _HOCL_iteratorSolution58.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator101 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution59 = (IteratorForSolution)_HOCL_tupleAtomIterator101.getAtomIterator(1);
		Molecule w_adapt_src = _HOCL_iteratorSolution59.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol42 = new Molecule();
		Tuple tuple36 = new Tuple(2);
		tuple36.set(0, ExternalObject.getHOCL_TypeTranslation("SRC"));
		// new solution + 0 
		Solution solution32 = new Solution(); 
		{
			// new Molecule  
			Molecule mol43 = new Molecule();
			mol43.add(w_adapt_src);
			solution32.addMolecule(mol43);
		}
		tuple36.set(1, solution32);
		tuple = tuple36;
		mol42.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple37 = new Tuple(2);
		tuple37.set(0, ExternalObject.getHOCL_TypeTranslation("SRC_CONTROL"));
		// new solution + 0 
		Solution solution33 = new Solution(); 
		{
			// new Molecule  
			Molecule mol44 = new Molecule();
			mol44.add(w_adapt_src_control);
			solution33.addMolecule(mol44);
		}
		tuple37.set(1, solution33);
		tuple = tuple37;
		mol42.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple38 = new Tuple(2);
		tuple38.set(0, ExternalObject.getHOCL_TypeTranslation("IN"));
		// new solution + 0 
		Solution solution34 = new Solution(); 
		{
			// new Molecule  
			Molecule mol45 = new Molecule();
			solution34.addMolecule(mol45);
		}
		tuple38.set(1, solution34);
		tuple = tuple38;
		mol42.add(tuple);
		this.addType("Tuple");
		
		
		return mol42;
	}

} // end of class Gw_swap_src
