/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import fr.inria.hocl.core.hocli.ExternalObject;
import fr.inria.hocl.core.hocli.ReactionRule;
import fr.inria.hocl.core.hocli.Solution;
import fr.inria.hocl.core.hocli.Tuple;

/**
 * 
 * workflow.
 * 
 * @author msimonin
 *
 */
public class Workflow extends GinflowTuple implements Serializable{

    /** Default serial id.*/
    private static final long serialVersionUID = 1L;
    
    /** Internal Id.*/
    private String id_;
    
    /** Service list.*/
    private List<String> servicesNames_;
    
    /** Terminal services list (build from WorkflowBuilder.build()).*/
    private List<String> terminalServices_ ;
    
    private List<RebranchingInformation> rebranchingInformations_;
    
    public static Workflow newWorkflow(String key) {
        return new Workflow(key, UUID.randomUUID().toString());
    }
    
    public static Workflow newWorkflow(String key, String workflowId) {
        return new Workflow(key, workflowId);
    }
    
    
    private Workflow(String key, String workflowId) {
    	super(key);
    	id_ = workflowId;
    	terminalServices_ = new ArrayList<String>();
        servicesNames_ = new ArrayList<String>();
        rebranchingInformations_ = new ArrayList<RebranchingInformation>();
    }
    
    // TODO extract common interface with service
    /**
     * 
     * Sets an arbitrary ginflow tuple to the value.
     * "Key":&lt;v1, v2 ...&gt;
     * Idempotent operation : remove existing one.
     * 
     * O(size value)
     * 
     * @param tuple    The tuple
     * @return the current workflow
     */
    public Workflow setGinflowTuple(GinflowTuple tuple) {
        Tuple innerTuple = has(tuple.getKey());
        if (innerTuple != null){
            remove(innerTuple);
        }
        addAtom(tuple);
        return this;
    }

    public void remove(Tuple  gTuple) {
        getValue().remove(gTuple);
    }
    
    /**
     * 
     * Sets the name of the workflow.
     * 
     * @param name	The name of the workflow
     */
    public void setName(String name) {
        setKey(name);
    }
    /**
     * 
     * Gets the service or a clone of.
     * Since underlying solution may change 
     * It is safe to see the underlying service description : "Si":&lt;&gt;
     * as a Tuple (not a GinflowTuple, nor a Service instanceof).
     * 
     * @param key	The key
     * 
     * @return  The service (or a copy of).
     */
    public Service getService(String key) {
        Tuple serviceTuple = has(new ExternalObject(key));
        Service service = null;
        if (serviceTuple != null) {
            try{
                // we try the catch
                service = (Service) serviceTuple;
            }
            catch(Exception e) {
                // create an image of the underlying service description
                service = Service.newService(serviceTuple);
            }
        }
        return service;
    }
    
    public Workflow setService(String key, Solution solution) {
        GinflowTuple service = Service.newService(key);
        service.setValue(solution);
        registerService(key, service);
        return this;
    }
    
    /**
     * 
     * Add a new Service to the workflow.
     * Idempotent : will overwrite existing service.
     * 
     * O(size)
     * 
     * @param key		The key
     * @param service	The service
     * @return	The workflow under construction.
     */
    public Workflow registerService(String key, GinflowTuple service) {
    	if (!servicesNames_.contains(key)) {
    		// if the service already exists
    		// don't change it's nature (terminal or not -> in case of update)
    		// but overwrite its description -
	        servicesNames_.add(key);
	        if ((! service.hasValues(GinflowType.DST)) && ( ! service.hasValues(GinflowType.DST_CONTROL))) {
	        	// the service is terminal
	        	terminalServices_.add(key);
	        }
    	}
        return setGinflowTuple(service);
    }
    
    public Workflow addRule(ReactionRule rule) {
        this.addAtom(rule);
        return this;
    }

    /**
     * @return the terminalServices
     */
    public List<String> getTerminalServices() {
        return terminalServices_;
    }

    /**
     * @param terminalServices the terminalServices to set
     */
    public void setTerminalServices(List<String> terminalServices) {
        terminalServices_ = terminalServices;
    }


   
    /**
     * @return the servicesNames
     */
    public List<String> getServicesNames() {
        return servicesNames_;
    }

    /**
     * @param servicesNames the servicesNames to set
     */
    public void setServicesNames(List<String> servicesNames) {
        servicesNames_ = servicesNames;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id_;
    }

    /**
     * @return the rebranchingInformation
     */
    public List<RebranchingInformation> getRebranchingInformations() {
        return rebranchingInformations_;
    }

    /**
     *        
     * 
     * @param rebranchingInformations	The rebranching informations
     */
    public void setRebranchingInformations(
            List<RebranchingInformation> rebranchingInformations) {
        rebranchingInformations_ = rebranchingInformations;
    }
    
    public void addRebranchingInformation(RebranchingInformation rebranchingInformation) {
    	rebranchingInformations_.add(rebranchingInformation);
    }

	public boolean hasRebranchingInformation() {
		return !rebranchingInformations_.isEmpty();
	}

	public Service getOrSetService(String sName) {
		Service service = getService(sName);
		if (service == null) {
			// service not found, set it.
			service = Service.newEmptyService(sName);
			registerService(sName, service);
		}
		return service;
	}

   

}
