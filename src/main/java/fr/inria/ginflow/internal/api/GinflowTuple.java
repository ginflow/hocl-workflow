/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import java.util.List;

import fr.inria.hocl.core.hocli.Atom;
import fr.inria.hocl.core.hocli.ExternalObject;
import fr.inria.hocl.core.hocli.Molecule;
import fr.inria.hocl.core.hocli.SimpleIterator;
import fr.inria.hocl.core.hocli.Solution;
import fr.inria.hocl.core.hocli.Tuple;

/**
 * 
 * A GinflowTuple is a particular tuple of shape : "KEY":&lt;?w&gt; key is "KEY" value
 * is &lt;?w&gt;
 * 
 * 
 * @author msimonin
 * 
 */
public class GinflowTuple extends Tuple {

    /** Default Serial id. */
    private static final long serialVersionUID = 1L;

    /** The internal key. */
    protected ExternalObject key_;

    /** The internal solution. */
    protected Solution value_;

    public GinflowTuple(String key) {
        super(2);
        key_ = new ExternalObject(key);
        this.set(0, key_);
        value_ = new Solution();
        this.set(1, value_);
    }

    public GinflowTuple() {
        super(2);
        new GinflowTuple("");
    }

    public GinflowTuple(String key, List<Object> values) {
        super(2);
        key_ = new ExternalObject(key);
        this.set(0, key_);
        value_ = new Solution();
        this.set(1, value_);
        this.addAllValues(values);
    }

    /**
     * 
     * Create a image of a tuple as a GinflowTuple. It is assumed that the tuple
     * has been tested with isGinflowTuple.
     * 
     * @param tuple	The tuple
     */
    public GinflowTuple(Tuple tuple) {
        super(2);
        key_ = (ExternalObject) tuple.get(0);
        this.set(0, key_);
        value_ = (Solution) tuple.get(1);
        this.set(1, value_);
    }

    protected void addAtom(Atom atom) {
        value_.addAtom(atom);
    }

    protected void addMolecule(Molecule molecule) {
        value_.addMolecule(molecule);
    }

    protected boolean contains(Atom atom) {
        return value_.contains(atom);
    }

    /**
     * @return the size of the value.
     */
    public int valueSize() {
        return value_.size();
    }

    /**
     * @return the key
     */
    public ExternalObject getKey() {
        return (ExternalObject) this.get(0);
    }

    /**
     * @param key
     *            the key to set
     * 
     */
    public void setKey(ExternalObject key) {
        this.set(0, key);
    }

    /**
     * @param key
     *            the key to set
     * 
     */
    public void setKey(String key) {
        this.set(0, new ExternalObject(key));
    }

    /**
     * @return the value
     */
    public Solution getValue() {
        return value_;
    }
    
    /**
     * @return the value
     */
    public Atom getFirst() {
        if (value_.size() <= 0 ) {
            return null;
        }
        SimpleIterator<Atom> it = value_.newIterator();
        return it.next();
    }

    /**
     * @param value
     *            the value to set
     * 
     */
    public void setValue(Solution value) {
        // updating the value_
        this.set(1, value);
        value_ = value;
    }

    public void addValue(Object value) {
        value_.addAtom(new ExternalObject(value));
    }

    public void addAllValues(List<Object> values) {
        for (Object value : values) {
            addValue(value);
        }
    }

    public static boolean isGinflowTuple(Atom atom) {
        if (!(atom instanceof Tuple)) {
            return false;
        }
        return isGinflowTuple((Tuple) atom);
    }

    public static boolean isGinflowTuple(Tuple tuple) {
        if (tuple instanceof GinflowTuple) {
            return true;
        }

        String key;
        Solution value;
        try {
            key = (String) ((ExternalObject) tuple.get(0)).getObject();
            value = (Solution) tuple.get(1);
            return true;

        } catch (Exception e) {
            return false;
        }

    }

    /**
     * 
     * Check if the current contains a Ginflowtuple like tuple whose key is key.
     * TODO returns a GinflowTuple image of the Tuple (like it is done in
     * workflow.getService())
     * 
     * 
     * @param key
     *            The key to use.
     * @return The matching tuple if any, null otherwise.
     */
    public Tuple has(ExternalObject key) {
        SimpleIterator<Atom> it = getValue().contents.newIterator();
        Atom atom;
        while ((atom = it.next()) != null) {
            if (GinflowTuple.isGinflowTuple(atom)) {
                Tuple gtuple = (Tuple) atom;
                ExternalObject gtupleKey = (ExternalObject) gtuple.get(0);
                if (gtupleKey == null)
                    System.out.println(this);
                if (gtupleKey.equals(key)) {
                    return gtuple;
                }
            }
        }
        return null;
    }

    /**
     * 
     * Wrapper
     * 
     * @param key	The key 
     * @return The matching tuple if any, null otherwise.
     */
    public Tuple has(String key) {
        return has(new ExternalObject(key));
    }

    /**
     * 
     * Gets an iterator on the underlying atom.
     * 
     * @return the iterator
     */
    public SimpleIterator<Atom> newIterator() {
        return getValue().newIterator();
    }

	public boolean hasValues(String key) {
		Tuple dst = this.has(key);
		if (dst != null) {
			GinflowTuple gTuple = new GinflowTuple(dst);
			return gTuple.getValue().size() != 0;
		}
		return false; 
	}


}
