/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import java.io.Serializable;

import fr.inria.hocl.core.hocli.ExternalObject;

/**
 * 
 * Name tuple. "NAME":&lt;src1, ..., srcn&gt;
 * 
 * @author msimonin
 *
 */
public class Name extends GinflowTuple implements Serializable {

	/** Default serial id. */
	private static final long serialVersionUID = 1L;

	/**
	 * Build a new name.
	 * 
	 * @param name	The name of the ginflow tuple
	 * @return	The newly created ginflow tuple. 
	 */
	public static Name newName(String name) {
		return new Name(name);
	}

	private Name(String name) {
		super(String.valueOf(GinflowType.NAME));
		this.add(name);
	}

	/**
	 * Add a String element to the values.
	 * 
	 * @param value
	 * @return the current GinflowTuple
	 */
	private Name add(String value) {
		addAtom(new ExternalObject(value));
		return this;
	}

	/**
	 * 
	 * Check wheter the names contains the value.
	 * 
	 * @param value
	 *            The value to check
	 * @return true iff the value is found
	 */
	public boolean contains(String value) {
		return contains(new ExternalObject(value));
	}
}
