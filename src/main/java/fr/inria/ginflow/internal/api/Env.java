/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import fr.inria.hocl.core.hocli.ExternalObject;

/**
 * 
 * Source "IN":&lt;in1, ..., inn&gt;
 * 
 * @author msimonin
 * 
 */
public class Env extends GinflowTuple {

	/** Default serial id. */
	private static final long serialVersionUID = 1L;

	/**
	 * Build a new in.
	 * 
	 * @return The newly created env
	 */
	public static Env newEnv() {
		return new Env();
	}

	private Env() {
		super(String.valueOf(GinflowType.ENV));
	}

	/**
	 * 
	 * Add a environment variable given has a key, value tuple.
	 * 
	 * @param key
	 *            The key to add to the environment
	 * @param value
	 *            The associated value
	 * @return the current Env
	 */
	public Env add(String key, String value) {
		String envDescription = String.format("%s=%s", key, value);
		value_.addAtom(new ExternalObject(envDescription));
		return this;
	}

	/**
	 * Add a environment variable.
	 * 
	 * @param keyEqualValue
	 *            "key=value" string
	 * @return	The env under construction
	 */
	public Env add(String keyEqualValue) {
		value_.addAtom(new ExternalObject(keyEqualValue));
		return this;
	}

}
