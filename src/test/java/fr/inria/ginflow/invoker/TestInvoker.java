/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.invoker;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.GinflowType;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.hocl.core.hocli.ExternalObject;
import fr.inria.hocl.core.hocli.Molecule;
import fr.inria.hocl.core.hocli.Solution;
import fr.inria.hocl.core.hocli.Tuple;

/**
 * @author msimonin
 * 
 */
@Test(groups = { "main" })
public class TestInvoker {


    @Test(enabled = true)
    public void test_invoker_shape_result() {
        Molecule inputParameters = new Molecule();
        String command = "echo foo";
        Solution solution = Invoker.call("T1", command, inputParameters, null);
        // we mimic the encapsulaltion in a tuple "RES":Invoker.call(command, inputParameters)
        GinflowTuple gTuple = new GinflowTuple("RES");
        gTuple.setValue(solution);
        Assert.assertNotNull(gTuple.has(GinflowType.OUT));
        Assert.assertNotNull(gTuple.has(GinflowType.ERR));
        Assert.assertNotNull(gTuple.has(GinflowType.EXIT));
    }

    
    @Test(enabled = true)
    public void test_invoker_result_echo_foo() {
        Molecule inputParameters = new Molecule();
        String command = "echo foo";
        Solution solution = Invoker.call("T1", command, inputParameters, null);
        // we mimic the encapsulation in a tuple "RES":Invoker.call(command, inputParameters)
        // we use GTUPLE to have the has method...(instead of iterating on the atoms)
        GinflowTuple gTuple = new GinflowTuple("RES");
        gTuple.setValue(solution);
        
        // one output
        Tuple out = gTuple.has(GinflowType.OUT);
        Assert.assertEquals(out.get(1).toString(), "<\"foo\">");

        // no error
        Tuple err = gTuple.has(GinflowType.ERR);
        Assert.assertEquals(err.get(1).toString(), "<\"\">");
        
        // exit
        Tuple exit = gTuple.has(GinflowType.EXIT);
        Assert.assertEquals(exit.get(1).toString(), "<0>");
    }
    
    @Test(enabled = true)
    public void test_invoker_result_command_not_found() {
        Molecule inputParameters = new Molecule();
        String command = "echoz foo";
        Solution solution = Invoker.call("T1", command, inputParameters, null);
        // we mimic the encapsulation in a tuple "RES":Invoker.call(command, inputParameters)
        // we use GTUPLE to have the has method...(instead of iterating on the atoms)
        GinflowTuple gTuple = new GinflowTuple("RES");
        gTuple.setValue(solution);
        System.out.println(gTuple);
        
        // one output
        Tuple out = gTuple.has(GinflowType.OUT);
        Assert.assertEquals(out.get(1).toString(), "<\"\">");

        // The implementation is throwing an exception, here we get the exception.getMessage();
        Tuple err = gTuple.has(GinflowType.ERR);
        Assert.assertTrue(err.get(1).toString().contains("command not found"));
        
        // exit isn't set due to the exception thrown.
        Tuple exit = gTuple.has(GinflowType.EXIT);
        Assert.assertEquals(exit.get(1).toString(), "<127>");
    }
    
    
    @Test(enabled = true)
    public void test_invoker_result_echo_$foo_with_env() {
        Molecule inputParameters = new Molecule();
        Molecule envMolecule = new Molecule();
        ExternalObject var1 = new ExternalObject("foo=bar");
        envMolecule.add(var1);
        String command = "echo $foo";
        Solution solution = Invoker.call("T1", command, inputParameters, envMolecule);
        // we mimic the encapsulation in a tuple "RES":Invoker.call(command, inputParameters)
        // we use GTUPLE to have the has method...(instead of iterating on the atoms)
        GinflowTuple gTuple = new GinflowTuple("RES");
        gTuple.setValue(solution);
        
        // one output
        Tuple out = gTuple.has(GinflowType.OUT);
        Assert.assertEquals(out.get(1).toString(), "<\"bar\">");

        // no error
        Tuple err = gTuple.has(GinflowType.ERR);
        Assert.assertEquals(err.get(1).toString(), "<\"\">");
        
        // exit
        Tuple exit = gTuple.has(GinflowType.EXIT);
        Assert.assertEquals(exit.get(1).toString(), "<0>");
    }
    
    @Test(enabled = true)
    public void test_invoker_deconstruct_environment(){
        Molecule envMolecule = new Molecule();
        ExternalObject var1 = new ExternalObject("foo=bar");
        ExternalObject var2 = new ExternalObject("fofo=baba");
        envMolecule.add(var1);
        envMolecule.add(var2);
        Map<String,String> envs = Invoker.deconstruct(envMolecule);
        Assert.assertNotNull(envs.get("foo"));
        Assert.assertEquals(envs.get("foo"), "bar");
        Assert.assertNotNull(envs.get("fofo"));
        Assert.assertEquals(envs.get("fofo"), "baba");
    }
    
    @Test(enabled = true)
    public void test_invoker_deconstruct_empty_environment(){
        Molecule envMolecule = new Molecule();
        Map<String,String> envs = Invoker.deconstruct(envMolecule);
        Assert.assertEquals(envs.size(), 0);
    }
    
    @Test(enabled = true)
    public void test_invoker_deconstruct_null_environment(){
        Map<String,String> envs = Invoker.deconstruct(null);
        Assert.assertEquals(envs.size(), 0);
    }
    
}
