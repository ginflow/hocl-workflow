/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.external.json;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.external.api.ServiceBuilder;
import fr.inria.ginflow.external.api.WorkflowBuilder;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.GinflowType;
import fr.inria.ginflow.internal.api.RebranchingInformation;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.options.OptionException;

/**
 * @author msimonin
 * 
 */
@Test(groups = { "main" })
public class TestDeserialization {
    

    //TODO fix with name ginflow tuple
    @Test(enabled = true)
    public void test_deserialize_one_service() throws JsonParseException, JsonMappingException, IOException, GinflowException {
        String json = "{\"name\":[\"1\"], \"src\":[\"1\", \"2\"], \"dst\":[\"2\"], \"srv\":[\"echo\"], \"in\":[\"!\"]}";
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new GinflowModule());
        ServiceBuilder sBuilder = mapper.readValue(json, ServiceBuilder.class);
        
        Service service = sBuilder.build();
        
        // Check name
        GinflowTuple name = new GinflowTuple(service.has(GinflowType.NAME));
        Assert.assertTrue(name.getValue().toString().contains("1"));
        Assert.assertEquals(service.getName(), "1");
        
        // Check source.
        GinflowTuple source = new GinflowTuple(service.has(GinflowType.SRC));
        Assert.assertTrue(source.getValue().toString().contains("1"));
        Assert.assertTrue(source.getValue().toString().contains("2"));
        // Check destination.
        GinflowTuple destination = new GinflowTuple(service.has(GinflowType.DST));
        Assert.assertTrue(destination.getValue().toString().contains("2"));
        // Check srv
        GinflowTuple srv = new GinflowTuple(service.has(GinflowType.SRV));
        Assert.assertTrue(srv.getValue().toString().contains("echo"));
        // Check in
        GinflowTuple in = new GinflowTuple(service.has(GinflowType.IN));
        Assert.assertTrue(in.getValue().toString().contains("!"));
    }
    
    //TODO fix with name ginflow tuple
    @Test()
    public void test_deserialize_workflow() throws JsonParseException, JsonMappingException, IOException, GinflowException {
        String json = "{\"name\":\"wf\", \"services\": [{\"name\":[\"1\"], \"src\":[], \"dst\":[], \"srv\":[\"echo\"], \"in\":[\"!\"]}, {\"name\":[\"2\"], \"src\":[], \"dst\":[], \"srv\":[\"echo\"], \"in\":[\"!\"]} ]}";
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new GinflowModule());
        WorkflowBuilder wBuilder = mapper.readValue(json, WorkflowBuilder.class);
            
        
        Workflow workflow = wBuilder.build();
        System.out.println(workflow.getRebranchingInformations());
        Assert.assertEquals(workflow.getValue().size(), 2);
    }
    

    @Test(groups = { "adapt" })
    public void test_build_workflow_with_adapt_from_json() throws JsonParseException, JsonMappingException, IOException, GinflowException, OptionException {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File("src/test/resources/adapt/adapt_version1.json");
        mapper.registerModule(new GinflowModule());
        WorkflowBuilder wBuilder = mapper.readValue(file, WorkflowBuilder.class);
        Workflow workflow = wBuilder.build();
        List<RebranchingInformation> rebranchingInformations = workflow.getRebranchingInformations();
        Assert.assertTrue(rebranchingInformations.size() == 1);
        RebranchingInformation rebranchingInformation = rebranchingInformations.get(0);
        Assert.assertEqualsNoOrder(rebranchingInformation.getSupervised().toArray(), new String[]{"2"});
        Assert.assertEqualsNoOrder(rebranchingInformation.getUpdateSrcControl().get("1").toArray(), new String[]{"4"});
        Assert.assertEqualsNoOrder(rebranchingInformation.getUpdateDstControl().get("3").toArray(), new String[]{"4"});
    }
    
    @Test()
    public void test_build_workflow_with_ri_from_json() throws JsonParseException, JsonMappingException, IOException, GinflowException, OptionException {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File("src/test/resources/adapt/adapt_version2.json");
        mapper.registerModule(new GinflowModule());
        WorkflowBuilder wBuilder = mapper.readValue(file, WorkflowBuilder.class);
        Workflow workflow = wBuilder.build();
        List<RebranchingInformation> rebranchingInformations = workflow.getRebranchingInformations();
        Assert.assertTrue(rebranchingInformations.size() == 1);
        RebranchingInformation rebranchingInformation = rebranchingInformations.get(0);
        Assert.assertEqualsNoOrder(rebranchingInformation.getSupervised().toArray(), new String[]{"2"});
        Assert.assertEqualsNoOrder(rebranchingInformation.getUpdateSrcControl().get("1").toArray(), new String[]{"4"});
        Assert.assertEqualsNoOrder(rebranchingInformation.getUpdateDstControl().get("3").toArray(), new String[]{"4"});
    }
    
    @Test(groups = { "adapt" })
    public void test_build_workflow_with_adapt_from_json_simple_to_fully() throws JsonParseException, JsonMappingException, IOException, GinflowException, OptionException {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File("src/test/resources/adapt/simple_to_fully.json");
        mapper.registerModule(new GinflowModule());
        WorkflowBuilder wBuilder = mapper.readValue(file, WorkflowBuilder.class);
        Workflow workflow = wBuilder.build();
        List<RebranchingInformation> rebranchingInformations = workflow.getRebranchingInformations();
        Assert.assertTrue(rebranchingInformations.size() == 1);
        RebranchingInformation rebranchingInformation = rebranchingInformations.get(0);
        Assert.assertEqualsNoOrder(rebranchingInformation.getSupervised().toArray(), new String[]{"2", "3", "4", "5"});
        Assert.assertEqualsNoOrder(rebranchingInformation.getUpdateSrcControl().get("1").toArray(), new String[]{"7", "8"});
        Assert.assertEqualsNoOrder(rebranchingInformation.getUpdateDstControl().get("6").toArray(), new String[]{"9", "10"});
        
    }
    
    @Test(groups = { "adapt" })
    public void test_build_workflow_with_adapt() throws JsonParseException, JsonMappingException, IOException, GinflowException, OptionException {
        WorkflowBuilder wb = WorkflowBuilder.newWorkflowBuilder();
        // sequence
        wb.setName("wf1");
        ServiceBuilder sb1 = ServiceBuilder.newBuilder();
        sb1.setName("s1");
        sb1.addAll(GinflowType.DST, Arrays.asList((Object)"s2"));
        ServiceBuilder sb2 = ServiceBuilder.newBuilder();
        sb2.setName("s2");
        sb2.addAll(GinflowType.DST, Arrays.asList((Object)"s3"));
        sb2.addAll(GinflowType.SRC, Arrays.asList((Object)"s1"));
        
        sb2.setSupervisedGroup(1);
        ServiceBuilder sb3 = ServiceBuilder.newBuilder();
        sb3.setName("s3");
        sb3.addAll(GinflowType.SRC, Arrays.asList((Object)"s2"));
        // alternatives
        ServiceBuilder sb4 = ServiceBuilder.newBuilder();
        sb4.setName("s4");
        sb4.addAll(GinflowType.DST, Arrays.asList((Object)"s3"));
        sb4.addAll(GinflowType.SRC, Arrays.asList((Object)"s1"));
        sb4.setAlternativeGroup(1);
        // alternatives
        ServiceBuilder sb5 = ServiceBuilder.newBuilder();
        sb5.setName("s5");
        sb5.addAll(GinflowType.DST, Arrays.asList((Object)"s3"));
        sb5.addAll(GinflowType.SRC, Arrays.asList((Object)"s1"));
        sb5.setAlternativeGroup(1);
        // register services
        wb.addServiceBuilder(sb1);
        wb.addServiceBuilder(sb2);
        wb.addServiceBuilder(sb3);
        wb.addServiceBuilder(sb4);
        wb.addServiceBuilder(sb5);
        // 
        Workflow wf = wb.build();
        
        List<RebranchingInformation> rebranchingInformations = wf.getRebranchingInformations();
        Assert.assertTrue(rebranchingInformations.size() == 1);
        RebranchingInformation rebranchingInformation = rebranchingInformations.get(0);
        // updateSrc
        Assert.assertEqualsNoOrder(rebranchingInformation.getUpdateSrc().get("s1").toArray(), new String[]{"s4", "s5"});
        // updateDst
        Assert.assertNotNull(rebranchingInformation.getUpdateDst().get("s3"));
        Assert.assertEqualsNoOrder(rebranchingInformation.getUpdateDst().get("s3").toArray(), new String[]{"s4", "s5"});
        
        // supervised
        Assert.assertEqualsNoOrder(rebranchingInformation.getSupervised().toArray(), new String[]{"s2"});
        
    }

}
