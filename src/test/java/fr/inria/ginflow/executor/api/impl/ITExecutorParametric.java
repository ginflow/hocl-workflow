/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import fr.inria.ginflow.distributed.communication.CommunicationFactory;
import fr.inria.ginflow.distributed.communication.embedded.api.EmbeddedBroker;
import fr.inria.ginflow.distributed.communication.embedded.api.impl.ActiveMQEmbeddedBroker;
import fr.inria.ginflow.distributed.communication.embedded.api.impl.KafkaEmbeddedBroker;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.executor.ExecutorFactory;
import fr.inria.ginflow.executor.api.GinflowExecutor;
import fr.inria.ginflow.executor.api.impl.TestExecutor;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.options.OptionException;
import fr.inria.ginflow.options.Options;

/**
 * @author msimonin
 * 
 */

@Test(groups = { "integration"})
public class ITExecutorParametric extends TestExecutor {

	private static Object[] executors;
	private static Object[] brokers;
	private static Map<String, EmbeddedBroker> test;
	private static ActiveMQEmbeddedBroker activeMQEmbeddedBroker_;
	private static EmbeddedBroker kafkaEmbeddedBroker_;

    @BeforeClass
    public static void setup() throws OptionException, FileNotFoundException, GinflowException {
    	// add mesos if wanted (and mesos configured correctly)
    	executors = new Object[]{"ssh"};
    	//brokers = new Object[]{"activemq", "kafka"};
    	brokers = new Object[]{"activemq"};
    	test = new HashMap<String, EmbeddedBroker>();
    	Options options = new Options();
    	
    	// Due to a bug with activeMQ embedded broker 
    	// we create a priori the embedded brokers.
    	activeMQEmbeddedBroker_ = new ActiveMQEmbeddedBroker(options);
    	activeMQEmbeddedBroker_.start();
    	kafkaEmbeddedBroker_ = new KafkaEmbeddedBroker(options);
    	kafkaEmbeddedBroker_.start();
    }
    
	@AfterClass
	public static void teardown() {
		activeMQEmbeddedBroker_.stop();
		kafkaEmbeddedBroker_.stop();	
	}
	
	@DataProvider(name="ExactTerminal")
	public Object [][] exactTerminalProvider() {
		/**
		 * index 1 : workflow
		 * index 2 : the out must be equal
		 * 
		 */
		Object[][] tests = provideExactTerminals();
		return buildProvidedTests(executors, brokers, tests);
	}

	
	
	
	@DataProvider(name="FuzzyTerminals")
	public Object [][] fuzzyTerminalsProvider() {
		/**
		 * 
		 * Allows to test the terminal (assumed to be of size 1)
		 * index 0 : workflow
		 * index 1 : what must be contained in the services results
		 * index 2 : what mustn't be contained in the services results.
		 */
		Object[][] tests = provideFuzzyTerminals();
	
		return buildProvidedTests(executors, brokers, tests);	
	}

	@DataProvider(name="adaptiveness")
	public Object [][] adaptivenessProvider() {
		Object[][] tests = provideAdaptiveness();
		return buildProvidedTests(new Object[]{"ssh"}, brokers, tests);
	}

	@Test(dataProvider = "adaptiveness")
    public void test_update_workflow(
    		String executor,
    		String broker,
    		Workflow[] workflows,
    		String[][] expectedResults,
    		String[][] unexpectedResults
    		) throws Exception {
    	
    	Options options = createOptions(executor, broker);
		
		GinflowExecutor ginflowExecutor = ExecutorFactory.newExecutor(workflows[0], executor, options);
		Assert.assertEquals(workflows[0].getTerminalServices().size(), 1);
		new Thread(new UpdateThread(ginflowExecutor, workflows[1])).start();
		
		ginflowExecutor.execute();
		
		System.out.println(workflows[0]);
		testResults(workflows[0], expectedResults, unexpectedResults);
    }
	
	@Test(dataProvider="FuzzyTerminals")
	public void testFuzzyTerminals(
		String executor, String broker, Workflow workflow, String[][] expectedResults, String[][] unexpectedResults) throws GinFlowExecutorException, FileNotFoundException, OptionException, GinflowException {
		
		
		Options options = createOptions(executor, broker);
		
		GinflowExecutor ginflowExecutor = ExecutorFactory.newExecutor(workflow, executor, options);
		
		ginflowExecutor.execute();
		testResults(workflow, expectedResults, unexpectedResults);
	}

	@Test(enabled = true, dataProvider="ExactTerminal")
	public void testExactTerminal(
		String executor, String broker, Workflow workflow, String[] expectedResult) throws GinFlowExecutorException, FileNotFoundException, OptionException, GinflowException {
		System.out.println(test.keySet());
		Options options = createOptions(executor, broker);
		
		GinflowExecutor ginflowExecutor = ExecutorFactory.newExecutor(workflow, executor, options);
		ginflowExecutor.execute();
		
        // get last result
		List<String> theTerminals = workflow.getTerminalServices();
		// in this test suite it is expected to have only one terminal
		Assert.assertEquals(theTerminals.size(), 1);
        Service service = workflow.getService(theTerminals.get(0));
        // parsing result.
        GinflowTuple[] result = service.getResult();
        // testing results
		// test result
		Assert.assertEquals(result[Service.OUT_INDEX].getValue().toString(), expectedResult[Service.OUT_INDEX]);
	    Assert.assertEquals(result[Service.ERR_INDEX].getValue().toString(), expectedResult[Service.ERR_INDEX]);
	    Assert.assertEquals(result[Service.EXIT_INDEX].getValue().toString(), expectedResult[Service.EXIT_INDEX]);
	}
	


	
	
	private Object[][] buildProvidedTests(Object[] executors, Object[] brokers, Object[][] tests) {
		// built the provided array.
		Object[][] result = new Object[executors.length * brokers.length * tests.length][];
		int i = 0;
		for (Object executor : executors) {
			for (Object broker : brokers) {
				Object[] src = {executor, broker};
				for (Object[] test : tests) {
					
					result[i] = new Object[2 + test.length];
					System.arraycopy(src, 0, result[i], 0, src.length);
					System.arraycopy(test, 0, result[i], src.length, test.length);
					
					i ++;
				}
			}
		}
		
		return result;
	}
	
	private void testResults(Workflow workflow, String[][] expectedResults, String[][] unexpectedResults) {
		for (String[] expectedResult : expectedResults) {
	        // get last result
	        Service service = workflow.getService(expectedResult[0]);
	        // parsing result.
	        GinflowTuple[] result = service.getResult();
            // testing results (out:<! i >)
            Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains(expectedResult[1]));
            Assert.assertTrue(result[Service.ERR_INDEX].getValue().toString().contains(expectedResult[2]));
            Assert.assertEquals(result[Service.EXIT_INDEX].getValue().toString(), expectedResult[3]);
		}
		for (String[] unexpectedResult : unexpectedResults) {
	        // get last result
	        Service service = workflow.getService(unexpectedResult[0]);
	        // parsing result.
	        GinflowTuple[] result = service.getResult();
            // testing results (out:<! i >)
            Assert.assertFalse(result[Service.OUT_INDEX].getValue().toString().contains(unexpectedResult[1]));
		}
	}
	
    class UpdateThread implements Runnable {

		private GinflowExecutor executor_;
		private Workflow update_;

		public UpdateThread(GinflowExecutor executor, Workflow update) {
			executor_ = executor;
			update_ = update;
		}

		@Override
		public void run() {
			try {
				// wait a little and launch the adaptation!
				System.out.println("[UdpateThread] waiting");
				Thread.sleep(10000);
				System.out.println("[UdpateThread] launch update");
				executor_.update(update_);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (GinFlowExecutorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
    	
    }
}
